import axios from 'axios';

export function create(url, data, onDone) {
	return axios
		.post(url, data)
		.then(function(response) {
			console.log(response);
			if (onDone) {
				onDone(null, response);
			}
		})
		.catch(function(error) {
			console.log(error);
			if (onDone) {
				onDone(error);
			}
		});
}

export function update(url, data, onDone) {
	return axios
		.patch(url, data)
		.then(function(response) {
			if (onDone) {
				onDone(null, response);
			}
		})
		.catch(function(error) {
			console.log(error);
			if (onDone) {
				onDone(error);
			}
		});
}
