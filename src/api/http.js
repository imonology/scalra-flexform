import axios from 'axios';
export function httpGet(url, onDone) {
	axios.get(url).then(response => {
		onDone(response);
	});
}
export function httpPost(url, data, onDone) {
	axios
		.post(url, data, {
			headers: {
				'Content-Type': 'application/json'
			}
		})
		.then(response => {
			onDone(response);
		});
}
