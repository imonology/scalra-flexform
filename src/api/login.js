import store from '@/store';
import { getToken } from '@/utils/auth';
export function register(account, password, email) {
	return new Promise((resolve, reject) => {
		store.dispatch('sendEvent', {
			name: '_ACCOUNT_REGISTER',
			param: { account, password, email },
			onDone: function(response) {
				if (response.err) {
					reject(response.err);
				} else {
					resolve(response.result);
				}
			},
			mathod: 'POST'
		});
	});
}

export function login(account, password) {
	return new Promise((resolve, reject) => {
		store.dispatch('sendEvent', {
			name: '_ACCOUNT_LOGIN',
			param: { account, password },
			onDone: function(response) {
				if (response.err) {
					reject(response.err);
				} else {
					resolve(response);
				}
			},
			mathod: 'POST'
		});
	});
}

export function getInfo(token = getToken()) {
	return new Promise((resolve, reject) => {
		store.dispatch('sendEvent', {
			name: '_ACCOUNT_GETDATA',
			param: { account: token, type: 'control' },
			onDone: function(response) {
				if (response.err) {
					reject(response.err);
				} else {
					resolve(response.result);
				}
			},
			mathod: 'POST'
		});
	});
}

export function logout(token) {
	return new Promise((resolve, reject) => {
		store.dispatch('sendEvent', {
			name: '_ACCOUNT_LOGOUT',
			param: {},
			onDone: function(response) {
				if (response.err) {
					reject(response.err);
				} else {
					resolve(response);
				}
			},
			mathod: 'POST'
		});
	});
}
