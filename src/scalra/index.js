/* eslint-disable indent */
/*
 *
 SR
 */
import axios from 'axios';
import SockJS from 'sockjs-client';
export default {
	install(Vue, options) {
		var onDone = function onDone(args) {
			// SR.safeCall(_onDone, args);
			if (Array.isArray(eventStack) && eventStack.length > 0) {
				//console.log("Ejecting eventStack...");
				for (var key in eventStack) {
					SR.sendEvent(
						eventStack[key].type,
						eventStack[key].para,
						eventStack[key].onDone,
						eventStack[key].method,
						eventStack[key].keep_callback
					);
				}
				eventStack = [];
			}
		};
		let server_type = 'sockjs';
		var connHandler = msg => console.log('connection status: ' + msg);
		connectSocket(server_type, socketDomain, connHandler, onDone);
		SR.loadAPI(() => console.log('API loaded'));

		// Add $plugin instance method directly to Vue components
		Vue.prototype.$SR = SR;
		Vue.prototype.$socket = socket;
	}
};

const sockjsUrl = () => {
	const { protocol, hostname, port } = window.location;
	return `${protocol}//${hostname}:${port}/sockjs`;
	// return `${protocol}//${hostname}:${+port - 8}/sockjs`;
};

const httpGet = function(url, onDone) {
	axios.get(url).then(response => {
		onDone(response);
	});
};
const httpPost = function(url, data, onDone) {
	axios
		.post(url, data, {
			headers: {
				'Content-Type': 'application/json'
			}
		})
		.then(response => {
			onDone(response);
		});
};

// helper to convert an object to a key-value pair string
// NOTE: cannot go one-level deep for the object, use the POST method for that
// src: http://stackoverflow.com/questions/6566456/how-to-serialize-a-object-into-a-list-of-parameters
let serialiseObject = function(obj) {
	let pairs = [];
	for (let prop in obj) {
		if (!obj.hasOwnProperty(prop)) {
			continue;
		}
		pairs.push(`${prop}=${obj[prop]}`);
	}
	return pairs.join('&');
};

let serverDomain = undefined;
let socketDomain = undefined;

var PORT_ENTRY = 8080;
var PORT_HTTP_INC = 0;
var PORT_HTTPS_INC = 1;

// see if it's secured connection
var secured = false;
if (typeof securedConn !== 'undefined' && securedConn === true) {
	secured = true;
} else if (window.location.protocol === 'https:') {
	secured = true;
}

const HEADER_PARA = 'P';
const HEADER_UPDATE = 'U';
const HEADER_EVENT = 'E';
let SR = {},
	responseHandlers = {},
	eventStack = [],
	connectType = 'sockjs',
	conn_options = undefined;

let socket = {};
const url = sockjsUrl();
let sockjs = new SockJS(url);

const connectSocket = function(server_type, socket_url, connHandler, onDone) {
	// handles when connection with server is established
	var onConnect = function() {
		if (typeof connHandler === 'function') connHandler('connect');

		console.log('websocket server connected: ' + socket_url);
		if (typeof onDone === 'function') onDone();
	};

	// disconnect event
	var onDisconnect = function(obj) {
		if (typeof connHandler === 'function') connHandler('disconnect');

		console.warn('disconnected... obj: ');
		console.warn(obj);

		// attempt to re-connect
		// if (typeof conn_options === 'object') {
		// 	removeEntry(currentEntry);
		// 	currentEntry = undefined;
		// 	SR.Warn('re-connecting websocket with options');
		// 	SR.Warn(conn_options);
		// 	SR.setSocketServer(conn_options);
		// }
	};

	sockjs.onopen = () => {
		socket.opened = true;
		// send cookie explicitly
		console.warn(document.cookie);
		sockjs.send(document.cookie);
		onConnect();
	};
	socket.sendJSON = function(obj) {
		sockjs.send(JSON.stringify(obj));
	};

	// On connection close
	// sockjs.onclose = onDisconnect;
	sockjs.onclose = () => (socket = undefined);

	// On receive message from server
	sockjs.onmessage = e => {
		// Get the content
		var obj = JSON.parse(e.data);
		onResponse(obj[HEADER_UPDATE], obj[HEADER_PARA]);
	};
};
// generic response callback for system-defined messages
let onResponse = function(type, para) {
	// avoid flooding if SR_PUBLISH is sending streaming data
	console.log('[' + type + '] received');
	switch (type) {
		//
		// pubsub related
		//
		// when a new published message arrives
		case 'SR_MSG':
			// handle server-published messages
			break;
		// case 'SR_PUBLISH':
		// 	if (onChannelMessages.hasOwnProperty(para.channel)) {
		// 		if (typeof onChannelMessages[para.channel] !== 'function') {
		// 			SR.Error(
		// 				'channel [' +
		// 					para.channel +
		// 					'] handler is not a function'
		// 			);
		// 		} else {
		// 			onChannelMessages[para.channel](para.msg, para.channel);
		// 		}
		// 	} else {
		// 		SR.Error(
		// 			'cannot find channel [' + para.channel + '] to publish'
		// 		);
		// 	}
		// 	return true;
		// // when a list of messages arrive (in array)
		// case 'SR_MSGLIST':
		// 	var msg_list = para.msgs;
		// 	if (
		// 		msg_list &&
		// 		msg_list.length > 0 &&
		// 		onChannelMessages.hasOwnProperty(para.channel)
		// 	) {
		// 		for (var i = 0; i < msg_list.length; i++)
		// 			onChannelMessages[para.channel](msg_list[i], para.channel);
		// 	}
		// 	return true;

		// // redirect to another webpage
		// case 'SR_REDIRECT':
		// 	window.location.href = para.url;
		// 	return true;
		// case 'SR_NOTIFY':
		// 	SR.Warn('SR_NOTIFY para: ');
		// 	SR.Warn(para);
		// 	console.log(onChannelMessages);
		// 	if (onChannelMessages.hasOwnProperty('notify'))
		// 		onChannelMessages['notify'](para, 'notify');
		// 	return true;
		// //
		// // login related
		// //
		// case 'SR_LOGIN_RESPONSE':
		// case 'SR_LOGOUT_RESPONSE':
		// 	replyLogin(para);
		// 	return true;
		case 'SR_MESSAGE':
			this.$message({
				type: 'success',
				message: para.msg
			});
			return true;
		case 'SR_WARNING':
			this.$message({
				type: 'warning',
				message: para.msg
			});
			return true;
		case 'SR_ERROR':
			this.$message({
				type: 'error',
				message: para.msg
			});
			return true;

		default:
			// check if custom handlers exist and can handle it
			if (responseHandlers.hasOwnProperty(type)) {
				var callbacks = responseHandlers[type];

				// extract rid if available
				var rid = undefined;
				if (para.hasOwnProperty('_rid') === true) {
					rid = para['_rid'];
					delete para['_rid'];
				}

				if (rid) {
					callbacks[rid](para, type);

					// remove callback once done
					if (rid !== 'keep') {
						delete callbacks[rid];
					}
				} else {
					// otherwise ALL registered callbacks will be called
					if (Object.keys(callbacks).length > 1)
						SR.Warn(
							'[' +
								type +
								'] no rid in update, dispatching to first of ' +
								Object.keys(callbacks).length +
								' callbacks'
						);

					// call the first in callbacks then remove it
					// so only one callback is called unless it's registered via the 'keep_callback' flag
					for (var key in callbacks) {
						callbacks[key](para, type);
						if (key !== 'keep') {
							delete callbacks[key];
							break;
						}
					}
				}

				return true;
			}

			// still un-handled
			console.error('onResponse: unrecongized type: ' + type);
			return false;
	}
};

SR.safeCall = function(callback) {
	var return_value = undefined;

	// first check if callback is indeed a function
	if (typeof callback !== 'function') {
		return return_value;
	}

	// call the callback with exception catching
	try {
		var args = Array.prototype.slice.call(arguments);
		return_value = callback.apply(this, args.slice(1));
	} catch (e) {
		console.error(e);
	}

	return return_value;
};

// generate local GUID
SR.getGUID = function() {
	return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
		var r = (Math.random() * 16) | 0,
			v = c == 'x' ? r : (r & 0x3) | 0x8;
		return v.toString(16);
	});
};

// get a numerical random number between 0 and 10000
SR.createID = function() {
	var rand = function() {
		var f = arguments[1] ? arguments[0] : 0;
		var t = arguments[1] ? arguments[1] : arguments[0];
		return Math.floor(Math.random() * (t - f) + f);
	};
	return rand(10000);
};

// send event by websocket or http get
SR.sendEvent = (name, param, onDone, method, keep_callback) => {
	console.log('calling...' + name);
	if (typeof param === 'function') {
		keep_callback = method;
		method = onDone;
		onDone = param;
		param = {};
	}
	param = param || {};

	// setup request id for identifying response handler
	var rid = SR.createID();
	if (typeof onDone === 'function') {
		if (responseHandlers.hasOwnProperty(name) === false)
			responseHandlers[name] = {};

		// whether this callback will be kept, in such case, only ONE callback is stored
		// also, no rid will be sent
		if (keep_callback === true) {
			rid = 'keep';
		} else {
			// store rid as part of request's parameter
			param._rid = rid;
		}

		// store callback, indexed by rid
		responseHandlers[name][rid] = onDone;
	}

	// send event by websocket(sockjs)
	// cache requests if sockets are not yet initialized
	if (
		conn_options ||
		(typeof connectType !== 'undefined' &&
			(connectType === 'sockjs' || connectType === 'socketio'))
	) {
		console.log('using sockjs...');
		console.log(socket);
		// check if we will send directly or queue the event after connection is established
		if (socket.opened) {
			console.log('sockjs opened');
			var obj = {};
			obj[HEADER_EVENT] = name;
			obj[HEADER_PARA] = param;
			socket.sendJSON(obj);
		} else {
			eventStack.push({
				type: name,
				para: param,
				onDone: onDone,
				method: method,
				keep_callback: keep_callback
			});
		}
		return;
	} else {
		// send event by HTTP(axios)
		if (!serverDomain) {
			SR.Error('no server defined, cannot send event');
			return;
		}

		// remove rid from para
		// TODO: find better approach
		if (param.hasOwnProperty('_rid')) {
			delete param['_rid'];
		}

		var req = serverDomain + '/event/' + name;
		//console.log('[' + method + '] sending request: ' + req);

		// send message via HTTP (default to POST)
		if (method === 'GET') {
			// append parameters
			req += param ? '?' + serialiseObject(param) : '';

			httpGet(req, function(resObj) {
				if (resObj === undefined) {
					SR.Log('No Response');
					return;
				}

				let type = resObj[HEADER_UPDATE];
				let para = resObj[HEADER_PARA];

				onResponse(type, para);
			});
		} else {
			httpPost(req, param, function(resObj) {
				if (
					typeof resObj === 'undefined' ||
					Object.keys(resObj).length === 0
				) {
					SR.Log('No Response');
					return;
				}
				let type = resObj[HEADER_UPDATE];
				let para = resObj[HEADER_PARA];

				onResponse(type, para);
			});
		}
	}
};

// set up server for RESTful calls
SR.setRESTServer = function(port_or_name, onDone) {
	// check correctness
	if (typeof onDone !== 'function') {
		onDone = undefined;
	}
	var ip_port = undefined;
	var conn_type = secured ? 'https' : 'http';

	// TODO: do not hard code port here
	// by port (directly)
	if (typeof port_or_name === 'number') {
		ip_port = `${SR.host.name}:${port_or_name +
			(secured ? PORT_HTTPS_INC : PORT_HTTP_INC)}`;
		serverDomain = conn_type + '://' + ip_port;
	} else {
		// by name (via entry server)
		var fullname = port_or_name.replace(/-/g, '/');
		serverDomain = `${conn_type}://${
			SR.host.name
		}:${PORT_ENTRY}/${fullname}`;
	}
	console.log('serverDomain: ' + serverDomain);
	SR.safeCall(onDone);
};

//
// API-related
//
var APIlist = [];

// load server-defined API
SR.loadAPI = function(onDone) {
	// clear API list
	SR.API = {};

	// build a specific API
	var generateAPI = function(name) {
		return function(args, onDone) {
			if (typeof args === 'function') {
				onDone = args;
				args = {};
			}

			console.log('calling API [' + name + ']...');

			// NOTE: by default callbacks are always kept
			SR.sendEvent(
				name,
				args,
				function(result) {
					if (result.err) {
						console.error(result.err);
						return SR.safeCall(onDone, result.err);
					}

					SR.safeCall(onDone, null, result.result);
				},
				undefined,
				true
			);
		};
	};

	SR.sendEvent('SR_API_QUERY', function(result) {
		if (result.err) {
			console.error(result.err);
			return SR.safeCall(onDone, result.err);
		}
		console.log('API available:' + result.result);
		APIlist = result.result;

		// load each API
		for (var i = 0; i < APIlist.length; i++) {
			var name = APIlist[i];
			SR.API[name] = generateAPI(name);
		}

		// get direct API calls (need to convert from string to function)
		// see: https://stackoverflow.com/questions/2573548/given-a-string-describing-a-javascript-function-convert-it-to-a-javascript-func#
		SR.sendEvent('SR_API_QUERY_DIRECT', function(result) {
			if (result.err) {
				console.error(result.err);
				return SR.safeCall(onDone, result.err);
			}

			console.log('direct API available: ' + Object.keys(result.result));
			var direct_APIlist = result.result;

			// re-generate each API function from string representation
			var valid_API = [];
			for (var name in direct_APIlist) {
				try {
					let func;
					eval('func = ' + direct_APIlist[name]);
					SR.API[name] = func;
					valid_API.push(name);
				} catch (e) {
					console.error(e);
				}
			}
			if (valid_API.length > 0) {
				console.log('direct API generated: ' + valid_API);
			}

			// done when both callback and direct function calls are loaded
			SR.safeCall(onDone);
		});
	});
};
