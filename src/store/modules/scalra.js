import Vue from 'vue';
import socket from './socket';
import store from '../../store';
import { httpGet, httpPost } from '@/api/http';
import { serialiseObject } from '@/utils/helper';
const HEADER_PARA = 'P';
const HEADER_UPDATE = 'U';
const HEADER_EVENT = 'E';
let SR = {},
	eventStack = [],
	connectType = 'sockjs',
	conn_options = undefined;

let serverDomain = undefined;
let socketDomain = undefined;

var PORT_ENTRY = 8080;
var PORT_HTTP_INC = 0;
var PORT_HTTPS_INC = 1;

// see if it's secured connection
var secured = false;
if (typeof securedConn !== 'undefined' && securedConn === true) {
	secured = true;
} else if (window.location.protocol === 'https:') {
	secured = true;
}

SR.safeCall = function(callback) {
	var return_value = undefined;

	// first check if callback is indeed a function
	if (typeof callback !== 'function') {
		return return_value;
	}

	// call the callback with exception catching
	try {
		var args = Array.prototype.slice.call(arguments);
		return_value = callback.apply(this, args.slice(1));
	} catch (e) {
		console.error(e);
	}

	return return_value;
};

// generate local GUID
SR.getGUID = function() {
	return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
		var r = (Math.random() * 16) | 0,
			v = c == 'x' ? r : (r & 0x3) | 0x8;
		return v.toString(16);
	});
};

// get a numerical random number between 0 and 10000
SR.createID = function() {
	var rand = function() {
		var f = arguments[1] ? arguments[0] : 0;
		var t = arguments[1] ? arguments[1] : arguments[0];
		return Math.floor(Math.random() * (t - f) + f);
	};
	return rand(10000);
};

// send event by websocket or http get
SR.sendEvent = (name, param, onDone, method, keep_callback, dispatch) => {
	// console.log('calling...' + name);
	if (typeof param === 'function') {
		keep_callback = method;
		method = onDone;
		onDone = param;
		param = {};
	}
	param = param || {};

	// setup request id for identifying response handler
	var rid = SR.createID();
	if (typeof onDone === 'function') {
		if (state.responseHandlers.hasOwnProperty(name) === false)
			state.responseHandlers[name] = {};

		// whether this callback will be kept, in such case, only ONE callback is stored
		// also, no rid will be sent
		if (keep_callback === true) {
			rid = 'keep';
		} else {
			// store rid as part of request's parameter
			param._rid = rid;
		}

		// store callback, indexed by rid
		state.responseHandlers[name][rid] = onDone;
	}
	// console.log(state.responseHandlers);
	// send event by websocket(sockjs)
	// cache requests if sockets are not yet initialized
	if (
		conn_options ||
		(typeof connectType !== 'undefined' &&
			(connectType === 'sockjs' || connectType === 'socketio'))
	) {
		// check if we will send directly or queue the event after connection is established
		if (socket.state.connected) {
			var obj = {};
			obj[HEADER_EVENT] = name;
			obj[HEADER_PARA] = param;
			dispatch('sendJSON', obj);
		} else {
			dispatch(
				'stackEvent',
				{
					type: name,
					para: param,
					onDone: onDone,
					method: method,
					keep_callback: keep_callback
				},
				{ root: true }
			);
		}
		return;
	} else {
		// send event by HTTP(axios)
		if (!serverDomain) {
			SR.Error('no server defined, cannot send event');
			return;
		}

		// remove rid from para
		// TODO: find better approach
		if (param.hasOwnProperty('_rid')) {
			delete param['_rid'];
		}

		var req = serverDomain + '/event/' + name;
		//console.log('[' + method + '] sending request: ' + req);

		// send message via HTTP (default to POST)
		if (method === 'GET') {
			// append parameters
			req += param ? '?' + serialiseObject(param) : '';

			httpGet(req, function(resObj) {
				if (resObj === undefined) {
					SR.Log('No Response');
					return;
				}

				let type = resObj[HEADER_UPDATE];
				let para = resObj[HEADER_PARA];

				onResponse(type, para);
			});
		} else {
			httpPost(req, param, function(resObj) {
				if (
					typeof resObj === 'undefined' ||
					Object.keys(resObj).length === 0
				) {
					SR.Log('No Response');
					return;
				}
				let type = resObj[HEADER_UPDATE];
				let para = resObj[HEADER_PARA];

				onResponse(type, para);
			});
		}
	}
};

// set up server for RESTful calls
SR.setRESTServer = function(port_or_name, onDone) {
	// check correctness
	if (typeof onDone !== 'function') {
		onDone = undefined;
	}
	var ip_port = undefined;
	var conn_type = secured ? 'https' : 'http';

	// TODO: do not hard code port here
	// by port (directly)
	if (typeof port_or_name === 'number') {
		ip_port = `${SR.host.name}:${port_or_name +
			(secured ? PORT_HTTPS_INC : PORT_HTTP_INC)}`;
		serverDomain = conn_type + '://' + ip_port;
	} else {
		// by name (via entry server)
		var fullname = port_or_name.replace(/-/g, '/');
		serverDomain = `${conn_type}://${
			SR.host.name
		}:${PORT_ENTRY}/${fullname}`;
	}
	console.log('serverDomain: ' + serverDomain);
	SR.safeCall(onDone);
};

//
// API-related
//
var APIlist = [];
var generateAPI = function(name, dispatch) {
	return function(args, onDone) {
		if (typeof args === 'function') {
			onDone = args;
			args = {};
		}

		console.log('calling API [' + name + ']...');

		// NOTE: by default callbacks are always kept
		SR.sendEvent(
			name,
			args,
			function(result) {
				if (result.err) {
					console.error(result.err);
					return SR.safeCall(onDone, result.err);
				}

				SR.safeCall(onDone, null, result.result);
			},
			undefined,
			true,
			dispatch
		);
	};
};

const state = {
	API: {},
	responseHandlers: {},
	api_loaded: false
};

const mutations = {};

const actions = {
	loadAPI(
		{ commit, dispatch },
		onDone = () => {
			console.log('api loaded');
			state.api_loaded = true;
			Vue.prototype.$SR = SR;
		}
	) {
		return new Promise((resolve, reject) => {
			// console.log('loading API..');
			dispatch('sendEvent', {
				name: 'SR_API_QUERY',
				param: {},
				onDone: function(result) {
					if (result.err) {
						console.error(result.err);
						return SR.safeCall(onDone, result.err);
					}
					// console.log('API available:' + result.result);
					APIlist = result.result;

					state.API = {};
					// load each API
					for (var i = 0; i < APIlist.length; i++) {
						var name = APIlist[i];
						state.API[name] = generateAPI(name, dispatch);
					}
					SR.API = state.API;
					// get direct API calls (need to convert from string to function)
					// see: https://stackoverflow.com/questions/2573548/given-a-string-describing-a-javascript-function-convert-it-to-a-javascript-func#
					SR.sendEvent(
						'SR_API_QUERY_DIRECT',
						function(result) {
							if (result.err) {
								console.error(result.err);
								return SR.safeCall(onDone, result.err);
							}

							// console.log(
							// 	'direct API available: ' +
							// 		Object.keys(result.result)
							// );
							var direct_APIlist = result.result;

							// re-generate each API function from string representation
							var valid_API = [];
							for (var name in direct_APIlist) {
								try {
									let func;
									eval('func = ' + direct_APIlist[name]);
									SR.API[name] = func;
									valid_API.push(name);
								} catch (e) {
									console.error(e);
								}
							}
							if (valid_API.length > 0) {
								// console.log(
								// 	'direct API generated: ' + valid_API
								// );
							}

							// done when both callback and direct function calls are loaded
							SR.safeCall(onDone);
						},
						null,
						null,
						null,
						dispatch
					);
				},
				method: undefined,
				keep_callback: undefined
			});
		});
	},
	sendEvent({ commit, dispatch }, event) {
		let { name, param, onDone, method, keep_callback } = event;
		SR.sendEvent(name, param, onDone, method, keep_callback, dispatch);
	},
	API({ commit, dispatch }, params) {
		dispatch('sendEvent', params);
	}
};
const scalra = {
	state,
	mutations,
	actions,
	module: {
		socket
	}
};

export default scalra;
