/* eslint-disable indent */
import SockJS from 'sockjs-client';
import scalra from './scalra';

const HEADER_PARA = 'P';
const HEADER_UPDATE = 'U';
const HEADER_EVENT = 'E';
let serverDomain = undefined;
let socketDomain = undefined;

var PORT_ENTRY = 8080;
var PORT_HTTP_INC = 0;
var PORT_HTTPS_INC = 1;
const sockjsUrl = () => {
	const { protocol, hostname, port } = window.location;
	return `${protocol}//${hostname}:${port}/sockjs`;
	// return `${protocol}//${hostname}:${+port - 8}/sockjs`;
};

// generic response callback for system-defined messages
let onResponse = function(type, para) {
	// avoid flooding if SR_PUBLISH is sending streaming data
	// console.log('[' + type + '] received');
	switch (type) {
		//
		// pubsub related
		//
		// when a new published message arrives
		case 'SR_MSG':
			// handle server-published messages
			break;
		// case 'SR_PUBLISH':
		// 	if (onChannelMessages.hasOwnProperty(para.channel)) {
		// 		if (typeof onChannelMessages[para.channel] !== 'function') {
		// 			SR.Error(
		// 				'channel [' +
		// 					para.channel +
		// 					'] handler is not a function'
		// 			);
		// 		} else {
		// 			onChannelMessages[para.channel](para.msg, para.channel);
		// 		}
		// 	} else {
		// 		SR.Error(
		// 			'cannot find channel [' + para.channel + '] to publish'
		// 		);
		// 	}
		// 	return true;
		// // when a list of messages arrive (in array)
		// case 'SR_MSGLIST':
		// 	var msg_list = para.msgs;
		// 	if (
		// 		msg_list &&
		// 		msg_list.length > 0 &&
		// 		onChannelMessages.hasOwnProperty(para.channel)
		// 	) {
		// 		for (var i = 0; i < msg_list.length; i++)
		// 			onChannelMessages[para.channel](msg_list[i], para.channel);
		// 	}
		// 	return true;

		// // redirect to another webpage
		// case 'SR_REDIRECT':
		// 	window.location.href = para.url;
		// 	return true;
		// case 'SR_NOTIFY':
		// 	SR.Warn('SR_NOTIFY para: ');
		// 	SR.Warn(para);
		// 	console.log(onChannelMessages);
		// 	if (onChannelMessages.hasOwnProperty('notify'))
		// 		onChannelMessages['notify'](para, 'notify');
		// 	return true;
		// //
		// // login related
		// //
		// case 'SR_LOGIN_RESPONSE':
		// case 'SR_LOGOUT_RESPONSE':
		// 	replyLogin(para);
		// 	return true;
		case 'SR_MESSAGE':
			this.$message({
				type: 'success',
				message: para.msg
			});
			return true;
		case 'SR_WARNING':
			this.$message({
				type: 'warning',
				message: para.msg
			});
			return true;
		case 'SR_ERROR':
			this.$message({
				type: 'error',
				message: para.msg
			});
			return true;

		default:
			// check if custom handlers exist and can handle it
			let responseHandlers = scalra.state.responseHandlers
			if (responseHandlers.hasOwnProperty(type)) {
				var callbacks = responseHandlers[type];

				// extract rid if available
				var rid = undefined;
				if (para.hasOwnProperty('_rid') === true) {
					rid = para['_rid'];
					delete para['_rid'];
				}

				if (rid) {
					callbacks[rid](para, type);

					// remove callback once done
					if (rid !== 'keep') {
						delete callbacks[rid];
					}
				} else {
					// otherwise ALL registered callbacks will be called
					if (Object.keys(callbacks).length > 1)
						console.warn(
							'[' +
								type +
								'] no rid in update, dispatching to first of ' +
								Object.keys(callbacks).length +
								' callbacks'
						);

					// call the first in callbacks then remove it
					// so only one callback is called unless it's registered via the 'keep_callback' flag
					for (var key in callbacks) {
						callbacks[key](para, type);
						if (key !== 'keep') {
							delete callbacks[key];
							break;
						}
					}
				}

				return true;
			}

			// still un-handled
			console.error('onResponse: unrecongized type: ' + type);
			return false;
	}
};

///

// const namespace = true;
const state = {
	socket: null,
	connected: false,
	error: '',
	eventStack: []
};

const mutations = {};

const actions = {
	initSocket({ commit, dispatch }) {
		// console.log('initialing socket..');
		const url = sockjsUrl();
		let sockjs = new SockJS(url);

		sockjs.onopen = () => {
			// console.log('socket connected..');
			state.connected = true;
			// send cookie explicitly
			sockjs.send(document.cookie);
			if (
				Array.isArray(state.eventStack) &&
				state.eventStack.length > 0
			) {
				// console.log(state.eventStack);
				for (var key in state.eventStack) {
					dispatch('sendEvent', {
						name: state.eventStack[key].type,
						param: state.eventStack[key].para,
						onDone: state.eventStack[key].onDone,
						method: state.eventStack[key].method,
						keep_callback: state.eventStack[key].keep_callback
					});
				}
				state.eventStack = [];
			}
		};
		sockjs.onmessage = e => {
			// Get the content
			var obj = JSON.parse(e.data);
			// console.log('onmessage')
			// console.log(obj)
			onResponse(obj[HEADER_UPDATE], obj[HEADER_PARA]);
		};

		sockjs.sendJSON = function(obj) {
			sockjs.send(JSON.stringify(obj));
		};

		sockjs.onclose = () => {
			state.socket = null;
			state.connected = false;
		};
		state.socket = sockjs;
	},
	sendJSON({ commit }, json) {
		// console.log('in socket.js calling sendJSON', json);
		state.socket.sendJSON(json);
	},
	stackEvent({ commit }, event) {
		// console.log('pushing event: ', event);
		state.eventStack.push(event);
	}
};

const getters = {
	SOCKET(state) {
		return state.socket;
	}
};
export default {
	state,
	mutations,
	actions,
	getters
};
