/* eslint-disable indent */
import SockJS from 'sockjs-client';
export default {
	install(Vue, options) {
		const HEADER_PARA = 'P';
		const HEADER_UPDATE = 'U';
		const HEADER_EVENT = 'E';
		const { connectType } = options.connectType || 'sockjs';
		const connHandler =
			options.onEvent ||
			function(msg) {
				SR.Log('connection status: ' + msg);
			};

		let socket = {};
		const url = sockjsUrl();
		let sockjs = new SockJS(url);

		// remove a given entry server
		// var removeEntry = function(entry) {
		// 	if (typeof entry === 'number' && entry < entryServers.length) {
		// 		entryServers.splice(entry, 1);
		// 		return true;
		// 	} else if (typeof entry === 'string') {
		// 		for (var i = 0; i < entryServers.length; i++) {
		// 			if (entryServers[i] === entry) {
		// 				entryServers.splice(i, 1);
		// 				SR.Log(
		// 					'remove entry: ' +
		// 						entry +
		// 						'. entries left: ' +
		// 						entryServers.length
		// 				);
		// 				return true;
		// 			}
		// 		}
		// 	}
		// 	return false;
		// };
		// handles when connection with server is established
		// const onConnect = () => {
		// 	if (typeof connHandler === 'function') {
		// 		connHandler('connect');
		// 	}

		// 	// SR.Log('websocket server connected: ' + socket_url);
		// 	if (typeof onDone === 'function') {
		// 		onDone();
		// 	}
		// };

		// disconnect event
		// var onDisconnect = function(obj) {
		// 	if (typeof connHandler === 'function') {
		// 		connHandler('disconnect');
		// 	}

		// 	SR.Warn('disconnected... obj: ');
		// 	SR.Warn(obj);

		// 	// attempt to re-connect
		// 	if (typeof conn_options === 'object') {
		// 		removeEntry(currentEntry);
		// 		currentEntry = undefined;
		// 		SR.Warn('re-connecting websocket with options');
		// 		SR.Warn(conn_options);
		// 		SR.setSocketServer(conn_options);
		// 	}
		// };
		sockjs.onopen = () => {
			socket.opened = true;
			// send cookie explicitly
			console.warn(document.cookie);
			sockjs.send(document.cookie);
			// onConnect();
		};
		socket.sendJSON = function(obj) {
			sockjs.send(JSON.stringify(obj));
		};

		// On connection close
		// sockjs.onclose = onDisconnect;
		sockjs.onclose = () => (Vue.prototype.$socket = undefined);

		// On receive message from server
		sockjs.onmessage = e => {
			// Get the content
			var obj = JSON.parse(e.data);
			onResponse(obj[HEADER_UPDATE], obj[HEADER_PARA]);
		};

		// Add $plugin instance method directly to Vue components
		Vue.prototype.$socket = socket;
	}
};

const sockjsUrl = () => {
	const { protocol, hostname, port } = window.location;
	return `${protocol}//${hostname}:${port}/sockjs`;
	// return `${protocol}//${hostname}:${+port - 8}/sockjs`;
};

let responseHandlers = {};

// generic response callback for system-defined messages
var onResponse = function(type, para) {
	// avoid flooding if SR_PUBLISH is sending streaming data
	SR.Log('[' + type + '] received');
	switch (type) {
		//
		// pubsub related
		//
		// when a new published message arrives
		case 'SR_MSG':
			// handle server-published messages
			break;
		// case 'SR_PUBLISH':
		// 	if (onChannelMessages.hasOwnProperty(para.channel)) {
		// 		if (typeof onChannelMessages[para.channel] !== 'function') {
		// 			SR.Error(
		// 				'channel [' +
		// 					para.channel +
		// 					'] handler is not a function'
		// 			);
		// 		} else {
		// 			onChannelMessages[para.channel](para.msg, para.channel);
		// 		}
		// 	} else {
		// 		SR.Error(
		// 			'cannot find channel [' + para.channel + '] to publish'
		// 		);
		// 	}
		// 	return true;
		// // when a list of messages arrive (in array)
		// case 'SR_MSGLIST':
		// 	var msg_list = para.msgs;
		// 	if (
		// 		msg_list &&
		// 		msg_list.length > 0 &&
		// 		onChannelMessages.hasOwnProperty(para.channel)
		// 	) {
		// 		for (var i = 0; i < msg_list.length; i++)
		// 			onChannelMessages[para.channel](msg_list[i], para.channel);
		// 	}
		// 	return true;

		// // redirect to another webpage
		// case 'SR_REDIRECT':
		// 	window.location.href = para.url;
		// 	return true;
		// case 'SR_NOTIFY':
		// 	SR.Warn('SR_NOTIFY para: ');
		// 	SR.Warn(para);
		// 	console.log(onChannelMessages);
		// 	if (onChannelMessages.hasOwnProperty('notify'))
		// 		onChannelMessages['notify'](para, 'notify');
		// 	return true;
		// //
		// // login related
		// //
		// case 'SR_LOGIN_RESPONSE':
		// case 'SR_LOGOUT_RESPONSE':
		// 	replyLogin(para);
		// 	return true;
		case 'SR_MESSAGE':
			this.$message({
				type: 'success',
				message: para.msg
			});
			return true;
		case 'SR_WARNING':
			this.$message({
				type: 'warning',
				message: para.msg
			});
			return true;
		case 'SR_ERROR':
			this.$message({
				type: 'error',
				message: para.msg
			});
			return true;

		default:
			// check if custom handlers exist and can handle it
			if (responseHandlers.hasOwnProperty(type)) {
				var callbacks = responseHandlers[type];

				// extract rid if available
				var rid = undefined;
				if (para.hasOwnProperty('_rid') === true) {
					rid = para['_rid'];
					delete para['_rid'];
				}

				if (rid) {
					callbacks[rid](para, type);

					// remove callback once done
					if (rid !== 'keep') {
						delete callbacks[rid];
					}
				}
				// otherwise ALL registered callbacks will be called
				else {
					if (Object.keys(callbacks).length > 1)
						SR.Warn(
							'[' +
								type +
								'] no rid in update, dispatching to first of ' +
								Object.keys(callbacks).length +
								' callbacks'
						);

					// call the first in callbacks then remove it
					// so only one callback is called unless it's registered via the 'keep_callback' flag
					for (var key in callbacks) {
						callbacks[key](para, type);
						if (key !== 'keep') {
							delete callbacks[key];
							break;
						}
					}
				}

				return true;
			}

			// still un-handled
			console.error('onResponse: unrecongized type: ' + type);
			return false;
	}
};
