// helper to convert an object to a key-value pair string
// NOTE: cannot go one-level deep for the object, use the POST method for that
// src: http://stackoverflow.com/questions/6566456/how-to-serialize-a-object-into-a-list-of-parameters
export function serialiseObject(obj) {
	let pairs = [];
	for (let prop in obj) {
		if (!obj.hasOwnProperty(prop)) {
			continue;
		}
		pairs.push(`${prop}=${obj[prop]}`);
	}
	return pairs.join('&');
}
// generate local GUID
export function getGUID() {
	return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
		var r = (Math.random() * 16) | 0,
			v = c == 'x' ? r : (r & 0x3) | 0x8;
		return v.toString(16);
	});
}
