/**
 * Created by jiachenpan on 16/11/18.
 */

export function isvalidUsername(str) {
	// English, digits, ['_', '@', '.', '-'] allowed
	const accountRegex = new RegExp('^[A-Za-z0-9_@.-]+$');
	return accountRegex.test(str.trim());
}

export function isExternal(path) {
	return /^(https?:|mailto:|tel:)/.test(path);
}
