import { shallowMount } from '@vue/test-utils';
import sfSelect from '@/components/SF-select.vue';

describe('sfSelect.vue', () => {
	it('renders props.msg when passed nothing', () => {
		const params = { id: 'gender' };
		const wrapper = shallowMount(sfSelect, {
			propsData: { params }
		});
		expect(wrapper.text()).toMatch('');
	});
	it('renders html well without unselected flag', () => {
		const params = {
			value: 'name',
			options: [
				{
					value: 'Female',
					text: 'Female'
				}
			]
		};
		const wrapper = shallowMount(sfSelect, {
			propsData: { params }
		});
		expect(wrapper.html()).toBe(
			'<select><!----> <option value="Female">Female</option></select>'
		);
	});
	it('renders html well', () => {
		const params = {
			value: 'name',
			unselected: true,
			options: [
				{
					value: 'Female'
				}
			]
		};
		const wrapper = shallowMount(sfSelect, {
			propsData: { params }
		});
		expect(wrapper.html()).toBe(
			'<select><option selected="selected" disabled="disabled">Select</option> <option value="Female">Female</option></select>'
		);
	});
	it('renders html with attributes well', () => {
		let params = { id: 'gender' };
		let wrapper = shallowMount(sfSelect, {
			propsData: { params }
		});
		expect(wrapper.html()).toBe('<select id="gender"><!----> </select>');
	});
});
