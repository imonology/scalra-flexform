import { shallowMount } from '@vue/test-utils';
import SFtextarea from '@/components/SF-textarea.vue';

describe('SFtextarea.vue', () => {
	it('renders props.params when passed', () => {
		const params = { value: 'textarea' };
		const wrapper = shallowMount(SFtextarea, {
			propsData: { params }
		});
		const value = wrapper.find('textarea').element.value;
		expect(value).toMatch(params.value);
	});
	it('renders props.msg when passed nothing', () => {
		const params = {};
		const wrapper = shallowMount(SFtextarea, {
			propsData: { params }
		});
		expect(wrapper.text()).toMatch('');
	});
	it('renders html well', () => {
		const params = { value: 'name' };
		const wrapper = shallowMount(SFtextarea, {
			propsData: { params }
		});
		expect(wrapper.html()).toBe('<textarea></textarea>');
	});
	it('renders html with attributes well', () => {
		let params = { value: 'name', for: 'username' };
		let wrapper = shallowMount(SFtextarea, {
			propsData: { params }
		});
		expect(wrapper.html()).toBe('<textarea for="username"></textarea>');
		params = { id: 'a001', value: 'name', for: 'username' };
		wrapper = shallowMount(SFtextarea, {
			propsData: { params }
		});
		expect(wrapper.html()).toBe(
			'<textarea id="a001" for="username"></textarea>'
		);
		const value = wrapper.find('textarea').element.value;
		expect(value).toMatch('name');
	});
});
