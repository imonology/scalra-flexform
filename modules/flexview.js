/*
	flexview.js

	allow data from multiple forms be joint together via a definition file for display & edit

*/

var l_name = 'flexview';
var l_form = SR.State.get('FlexFormMap');

// find relevant actual data that fits a column definition
var l_resolveColumns = function(def, raw_row) {
	var resolved_row = [];

	LOG.warn('def received:');
	LOG.warn(def);
	LOG.warn('raw_row:');
	LOG.warn(raw_row);

	for (var i = 0; i < def.length; i++) {
		var column_def = def[i];
		var form = l_getFormByName(column_def.from);
		if (form === undefined) {
			LOG.error(
				'form [' + column_def.from + '] cannot be found!',
				l_name
			);
			continue;
		}
		LOG.warn('form [' + column_def.from + '] found!', l_name);

		// find matching data
		for (var record_id in form.data.values) {
			var record = form.data.values[record_id];

			// all keys must match
			var keys = column_def.keys;
			for (var j = 0; j < keys.length; j++) {
				var key = keys[j];
				if (record[key] !== raw_row[key]) break;
			}
			if (j !== keys.length) continue;

			// for full match, we'll translate the row_row data into a mapped value
			// first check if the display name exists
			if (record.hasOwnProperty(column_def.display) === false) {
				LOG.error(
					'cannot find field name [' +
						column_def.display +
						'] to display in form [' +
						column_def.from +
						'], please check spelling',
					l_name
				);
				resolved_row.push('NOT FOUND');
			} else {
				var value = record[column_def.display];

				// check if special conversion is needed (for array, etc.)
				if (column_def.array === true) {
					try {
						value = JSON.parse(value);
					} catch (e) {
						LOG.error(e, l_name);
					}
				}
				// if 'pivot' flag exists, we'll put this value into separate column data
				if (column_def.pivot === true && value instanceof Array) {
					for (var k = 0; k < value.length; k++) {
						resolved_row.push(value[k]);
					}
				} else {
					resolved_row.push(value);
				}
			}

			// we only use the 1st matched result
			break;
		}
	}

	return resolved_row;
};

// query a given form for the given set of key_fields and return only unique matches
let l_queryGroupField = function(form, group_fields = []) {
	// TODO: error check, form must be valid form, key_fields must be array (as SR.API?)

	let result = Object.values(
		Object.values(form.data.values).reduce((result, value) => {
			let unique_key = Object.keys(value)
				.reduce((r, k) => {
					if (group_fields.includes(k)) {
						r = r.concat([k, value[k]]);
					}
					return r;
				}, [])
				.join('-');
			if (!result.hasOwnProperty(unique_key)) {
				result[unique_key] = Object.assign({}, value);
			}
			return result;
		}, {})
	);

	return result;
};

// get form by name
let l_getFormByName = function(form_name) {
	let form = undefined;
	for (let form_id in l_form) {
		if (l_form[form_id].name === form_name) {
			form = l_form[form_id];
			break;
		}
	}
	return form;
};

SR.API.add(
	'CreateFlexview',
	{
		view_model: 'object'
	},
	function(args, onDone) {
		LOG.sys('CreateFlexview called');
		LOG.sys(args);
		let overview = args.overview;
		let details = args.details;

		//
		// generate 'overview'
		//
		LOG.sys('overview:');
		LOG.sys(overview);

		let form = l_getFormByName(overview.from);
		if (form === undefined)
			return onDone(
				'overview form [' + overview.from + '] cannot be found',
				l_name
			);

		let overview_form = form;
		LOG.sys('overview_form:');
		LOG.sys(overview_form);

		// do query of group fields
		// if (overview.hasOwnProperty('group') && Array.isArray(overview.group))
		let result = l_queryGroupField(overview_form, overview.group);
		LOG.sys('overview group fields result:');
		LOG.sys(result);

		// go through definition to resolve content of each row
		let overview_result = [];
		overview_result.push(overview.titles);
		for (let i = 0; i < result.length; i++) {
			overview_result.push(l_resolveColumns(overview.columns, result[i]));
		}

		LOG.sys('overview_result:');
		LOG.sys(overview_result);

		//
		// generate 'details'
		//
		LOG.sys('details:');
		LOG.sys(details);

		form = l_getFormByName(details.from);
		if (form === undefined)
			return onDone(
				'details form [' + details.from + '] cannot be found',
				l_name
			);

		let details_form = form;
		LOG.sys('details_form:');
		LOG.sys(details_form);

		// do query of key fields
		result = l_queryGroupField(details_form, details.group);
		LOG.sys('details key fields result:');
		LOG.sys(result);

		// go through definition to resolve content of each row
		let details_result = [];
		details_result.push(details.titles);
		for (let i = 0; i < result.length; i++) {
			details_result.push(l_resolveColumns(details.columns, result[i]));
		}

		LOG.sys('details_result:');
		LOG.warn(details_result);

		onDone(null, { overview: overview_result, detail: details_result });
	}
);
