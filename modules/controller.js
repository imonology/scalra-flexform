const flexform = require('./flexform.js');
const AccountController = require('./account/controller');
module.exports = class Controller {
	constructor(name) {
		this.name = name;
		this.data = {
			fields: [],
			values: {},
			meta: {}
		};

		// private member
		Object.defineProperty(this, 'lastAction', {
			enumerable: false,
			writable: true
		});
		Object.defineProperty(this, '_selectedField', {
			enumerable: false,
			writable: true
		});
		this.lastAction = 'init';
	}

	setName(name) {
		this.name = name;
		this.lastAction = 'setName';
	}

	addField(field = {}) {
		if (this.data.fields.some(f => f.id === field.id)) {
			throw new Error(`Field id duplicated: ${field.id}`);
		}
		if (!(field.id && field.name)) {
			throw new Error(
				'A field at least has to have [id] and [name] properties'
			);
		}
		this.data.fields.push(field);
	}

	addValue(record = {}) {
		// check required fields
		this.data.fields.forEach(f => {
			if (f.required && !record.hasOwnProperty(f.id)) {
				throw new Error(`Missing required field: ${f.id}`);
			}
		});
		this.data.values[Object.keys(this.data.values).length] = record;
	}

	checkName() {
		if (!this.name) {
			throw new Error('Controller should be set name first');
		}
	}

	find(args = {}) {
		this.checkName();
		this._getMeta();
		new Promise((resolve, reject) => {
			let params = { name: this.name };
			if (this._selectField(args)) {
				params.show = this._selectField(args);
			}
			if (args.query) {
				params.query = args.query;
			}
			SR.API.QUERY_FORM(params, (err, result) => {
				if (err) {
					LOG.error(err);
					return reject(err);
				}
				result = Object.assign({}, result);
				this.data.values = result.data.values;
				this.data.fields = result.data.fields;
				resolve();
			});
		});
		this.lastAction = 'find';
		return this;
	}

	findOne(args) {
		this.checkName();
		this._getMeta();
		let params = {
			name: this.name,
			query: args.query,
			record_id: args.record_id
		};
		if (this._selectField(args)) {
			params.show = this._selectField(args);
		}
		
		// console.log('params =') ;
		// console.log(params);
		
		new Promise((resolve, reject) => {
			SR.API.QUERY_FORM(params, (err, result) => {
				if (err) {
					return reject(err);
				}
				this.data.values = {};
				if (Object.keys(result.data.values).length < 1) {
					resolve();
				} else {
					result = Object.assign({}, result);
					let first_key =
						args.record_id || Object.keys(result.data.values)[0];
					if (result.data.values[first_key]) {
						this.data.values[first_key] =
							result.data.values[first_key];
					}
					this.data.fields = result.data.fields;
					resolve();
				}
			});
		});
		this.lastAction = 'findOne';
		return this;
	}

	create(obj) {
		this.checkName();
		new Promise((resolve, reject) => {
			// TODO: remove fields excluded in model
			SR.API.UPDATE_FIELD(
				{
					form_name: this.name,
					values: obj
				},
				err => {
					if (err) {
						return reject(err);
					}
				}
			);
		});
		this.lastAction = 'create';
		return this;
	}

	update(obj) {
		this.checkName();
		this._getMeta();
		if (!obj.values || !obj.record_id) {
			throw new Error('Missing values or record_id');
		}
		new Promise((resolve, reject) => {
			SR.API.UPDATE_FIELD(
				{
					form_name: this.name,
					values: obj.values,
					record_id: obj.record_id
				},
				err => {
					if (err) {
						reject(err);
					}
				}
			);
		}).then(() => {
			this.find();
		});
		this.lastAction = 'update';
		return this;
	}

	destroy(obj) {
		this.checkName();
		if (obj.record_id === undefined) {
			throw new Error('Missing record_id');
		}
		new Promise((resolve, reject) => {
			SR.API.DELETE_FIELD(
				{
					form_name: this.name,
					record_id: obj.record_id
				},
				err => {
					if (err) {
						reject(err);
					}
				}
			);
		}).then(() => {
			this.find();
		});
		this.lastAction = 'destory';
		return this;
	}

	populated(args = {}) {
		let { selected_fields, with_values } = args;
		selected_fields = selected_fields || [];
		with_values = with_values || false;
		this._getMeta();
		this.checkName();
		if (selected_fields.length === 0) {
			selected_fields = this.data.fields.map(field => field.id);
		}
		this.data.fields.forEach(field => {
			if (
				(field.hasOwnProperty('model')) &&
				selected_fields.includes(field.id)
			) {
				let ref_controller;
				let type;
				
				if (field.hasOwnProperty('model')) {
					ref_controller = new Controller(field.model);
					type = 'model';
				}
				if (
					(field.model && field.model === '_account') 
				) {
					ref_controller = new AccountController();
					type = '_account';
				}
				ref_controller.find();
				// generate option
				field.option = Object.keys(ref_controller.data.values).map(
					k => {
						if(type === '_account') {
							return {
								text: ref_controller.data.values[k]['account'],
								value: k,
								roles: ref_controller.data.values[k]['roles'],
							}
						}
						if(type === 'model'){
							return {
								text: ref_controller.data.values[k][field.option_text],
								value: k
							};	
						} 
					}
				);
				// populate ref data
				if (with_values) {
					if (Object.values(this.data.values).length > 0) {
						Object.values(this.data.values).map(v => {
							if (
								field.type === 'choice' &&
								v.hasOwnProperty(field.id)
							) {
								let ref_id =
									typeof v[field.id] === 'string'
										? v[field.id]
										: v[field.id].id;
								let ref_data =
									ref_controller.data.values[ref_id];
								if (ref_data) {
									ref_data.id = ref_id;
									v[field.id] = ref_data;
								} else {
									v[field.id] = '';
								}
								return v;
							} else if (
								field.type === 'multichoice' &&
								v.hasOwnProperty(field.id)
							) {
								if (
									Array.isArray(v[field.id]) &&
									v[field.id].length > 0
								) {
									let populated_array = [];
									v[field.id].forEach(ref_id => {
										if (typeof ref_id === 'string') {
											let ref_data =
												ref_controller.data.values[
													ref_id
												];
											populated_array.push(ref_data);
										} else if (typeof ref_id === 'object') {
											populated_array.push(ref_id);
										}
									});
									v[field.id] = populated_array;
								}
							}
						});
					}
				}
			}
		});
		this.lastAction = 'populated';
		return this;
	}

	schema() {
		this.checkName();
		let form = flexform.l_get(null, this.name);
		this._getMeta();
		this.data.fields = form.data.fields;
		this.data.values = {};
		this.lastAction = 'schema';
		return this;
	}
	_selectField({ select, discard, ...args }) {
		let selectedFields;
		if (!select && discard && discard.length > 0) {
			selectedFields = Object.keys(this.schema().data.fields).filter(
				field => !discard.includes(field)
			);
		}
		if (select && select.length > 0) {
			selectedFields = select;
		}
		return selectedFields;
	}
	_getMeta() {
		if (
			SR &&
			SR.Flexform &&
			SR.Flexform.models &&
			SR.Flexform.models[this.name]
		) {
			this.data.meta = SR.Flexform.models[this.name].meta || {};
		}
	}
};
