const flexform = require('./flexform.js');
const Controller = require('./controller.js');
const fs = require('fs');
let modelDir = SR.path.join(SR.Settings.PROJECT_PATH, 'api', 'models');

UTIL.validatePath(SR.path.join(SR.Settings.PROJECT_PATH, 'api'));
UTIL.validatePath(modelDir);
const express = SR.Module['express'].app;
const bodyParser = require('body-parser');
express.use(
	bodyParser.json({
		limit: '50mb'
	})
);

express.use(
	bodyParser.urlencoded({
		limit: '50mb',
		extended: true
	})
);

let fileList = fs.readdirSync(modelDir).reduce(function(list, file) {
	let path = SR.path.join(modelDir, file);
	let name = file;
	let isDir = fs.statSync(path).isDirectory();
	if (!isDir) {
		return list.concat([{ path, name }]);
	}
}, []);
SR.Flexform.models = SR.Flexform.models || {};
fileList.forEach(model => {
	SR.Flexform.models[model.name.split('.')[0]] = require(model.path);
});
console.log('model' + SR.Flexform.models);
LOG.sys('Generating apis from models', 'Flexform');
Object.keys(SR.Flexform.models).forEach(name => {
	if (name === '_account' || name === 'dashboard') {
		return;
	}
	let model = SR.Flexform.models[name];
	express.get(`/api/${name}`, (req, res) => {
		let form = flexform.l_get(null, name); // Equivalent to SR.API.QUERY_FORM({ name }, ...
		if (!form) {
			return res.send(form);
		}

		form = JSON.parse(JSON.stringify(form)); // Avoid modification to memory

		var jq = SR.JobQueue.createQueue();
		var l_do_edit_field = function(para) {
			return function(onD) {
				SR.API.QUERY_FORM(para.para, (err, include_form) => {
					let option = form.data.fields[para.i].option;
					// option = {a: 1};
					let new_option = [];
					for (let record_id in include_form.data.values) {
						new_option.push({
							value:
								include_form.data.values[record_id][
									option.value
								] || '',
							text:
								include_form.data.values[record_id][
									option.text
								] || ''
						});
					}
					form.data.fields[para.i].option = new_option;
					onD();
				});
			};
		};

		for (let i in form.data.fields) {
			let field = form.data.fields[i];
			if (field.option && field.option.form) {
				jq.add(
					l_do_edit_field({
						i,
						para: {
							name: field.option.form,
							query: field.option.query || {}
						}
					})
				);
			} else if (
				form.data.fields[i].hasOwnProperty('model') 
			) {
				let controller = new SR.Flexform.controller(name);
				controller.find().populated();
				form.data = controller.data;
			}
		}
		jq.run(function(err) {
			let c = new SR.Flexform.controller(name);
			c.schema();
			form.data.meta = c.data.meta || {};
			return res.send(form);
		});
	});
	express.get(`/api/${name}/schema`, (req, res) => {
		let controller = SR.Flexform.controllers[name];
		controller.schema();
		res.send(controller);
	});
	express.get(`/api/${name}/:id`, (req, res) => {
		let controller = SR.Flexform.controllers[name];
		controller.findOne({ record_id: req.params['id'] }).populated();
		res.send(controller);
	});
	express.post(`/api/${name}/`, (req, res) => {
		let new_model = {};
		Object.keys(model.fields).forEach(key => {
			if (req.body.hasOwnProperty(key)) {
				new_model[key] = req.body[key];
			}
		});
		SR.API.UPDATE_FIELD(
			{
				form_name: name,
				values: new_model
			},
			(e, r) => (e ? res.send(e) : res.send(r))
		);
	});
	express.get(`/api/custom/${name}/:id`, (req, res) => {
		let controller = SR.Flexform.controllers[name];
		controller.findOne({record_id: req.params['id']}).populated();
		console.log(controller);
		res.send(controller);
	})

	express.patch(`/api/${name}/:id`, (req, res) => {
		let update_model = {};
		const id = req.params['id'];
		Object.keys(model.fields).forEach(key => {
			if (req.body.hasOwnProperty(key)) {
				update_model[key] = req.body[key];
			}
		});
		SR.API.UPDATE_FIELD(
			{
				form_name: name,
				record_id: id,
				values: update_model
			},
			(e, r) => (e ? res.send(e) : res.send(r))
		);
	});

	express.delete(`/api/${name}/:id`, (req, res) => {
		const id = req.params['id'];
		SR.API.DELETE_FIELD(
			{
				form_name: name,
				record_id: id
			},
			(e, r) => (e ? res.send(e) : res.send(r))
		);
	});
});
LOG.sys('Auto-generated api from models', 'Flexform');
