const fs = require('fs');
let modelDir = SR.path.join(SR.Settings.PROJECT_PATH, 'api', 'models');

UTIL.validatePath(SR.path.join(SR.Settings.PROJECT_PATH, 'api'));
UTIL.validatePath(modelDir);

let fileList = fs.readdirSync(modelDir).reduce(function(list, file) {
	let path = SR.path.join(modelDir, file);
	let name = file;
	let isDir = fs.statSync(path).isDirectory();
	if (!isDir) {
		return list.concat([{ path, name }]);
	}
}, []);
SR.Flexform.models = SR.Flexform.models || {};
fileList.forEach(model => {
	SR.Flexform.models[model.name.split('.')[0]] = require(model.path);
});
Object.keys(SR.Flexform.models).forEach(name => {
	let model = SR.Flexform.models[name];
	// check if there is a logic conflict
	const conflictField = Object.keys(model.fields).find(
		key =>
			model.fields[key].hasOwnProperty('required') &&
			model.fields[key].required === true &&
			model.fields[key].hasOwnProperty('show') &&
			model.fields[key].show === false
	);
	if (conflictField) {
		LOG.error(
			'Model initializing failed: field cannot be set with `required: true` and `show: false` at the same time',
			'Flexform: Model'
		);
		LOG.error(
			`Conflict found in ${name} at ${conflictField}`,
			'Flexform: Model'
		);
		throw new Error(
			'Model initializing failed: field cannot be set with `required: true` and `show: false` at the same time'
		);
	}

	// deal associations
	let fields = Object.keys(model.fields).map(key => {
		let field = JSON.parse(JSON.stringify(model.fields[key]));
		// if (model.fields[key].hasOwnProperty('model')) {
		// 	field.type = 'choice';
		// } else if (model.fields[key].hasOwnProperty('collection')) {
		// 	field.type = 'multichoice';
		// }
		return Object.assign({}, field, { id: key });
	});

	let meta = model.meta;
	if (name !== '_account') {
		SR.API.INIT_FORM({ name, fields, meta }, e => e && LOG.error(e));
	}
});
LOG.sys('Form initialized from models done', 'Flexform');
