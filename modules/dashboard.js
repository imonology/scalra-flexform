/* eslint-disable no-undef */
const fs = require('fs');

const express = SR.Module['express'].app;
const bodyParser = require('body-parser');
express.use(
	bodyParser.json({
		limit: '50mb'
	})
);

express.use(
	bodyParser.urlencoded({
		limit: '50mb',
		extended: true
	})
);
// syhu NOTE: 	code below completely copies content in /modules/account.js, which should not happen
//				will create potential bugs
/*
let role_path = SR.path.join(SR.Settings.PROJECT_PATH, 'api', '_role.js');
let role_sample_path = SR.path.join(
	SR.Settings.PROJECT_PATH,
	'node_modules',
	'scalra-flexform',
	'modules',
	'_role.js.sample'
);
let roles = {};
if (fs.existsSync(role_path)) {
	roles = require(role_path);
} else {
	fs.copyFileSync(role_sample_path, role_path);
}
roles = roles.map(role => {
	role.text = role.label;
	role.value = role.name;
	delete role.label;
	delete role.name;
	return role;
});

express.get('/api/_roles', (req, res) => {
	res.send(roles);
});

let account_fields = [
	{
		name: 'account',
		type: 'string',
		required: true,
		id: 'account',
		immutable: true
	},
	{
		name: 'password',
		type: 'password',
		required: true,
		id: 'password'
	},
	{
		name: 'email',
		type: 'string',
		required: true,
		id: 'email'
	},
	{
		name: 'name',
		type: 'string',
		required: true,
		id: 'name'
	},
	{
		name: 'roles',
		type: 'multichoice',
		required: false,
		id: 'roles',
		option: roles
	}
];
*/
/*
let extra_fields_path = SR.path.join(
	SR.Settings.PROJECT_PATH,
	'api',
	'models',
	'dashboard.js'
);
let extra_fields = [];


let _account_keep_words = account_fields.map(f => f.id);
if (fs.existsSync(extra_fields_path)) {
	extra_fields = require(extra_fields_path).fields;
	extra_fields = Object.keys(extra_fields)
		.map(k => {
			return Object.assign({}, extra_fields[k], {
				id: k
			});
		})
		.filter(field => {
			return !_account_keep_words.includes(field.id);
		});
	console.log('extra_fields' + JSON.stringify(extra_fields));
}
module.exports.account_fields = account_fields;
*/

express.get('/api/dashboard/schema', (req, res) => {
	let _account_controller = SR.Flexform.controllers['dashboard'];
	_account_controller.schema();
	_account_controller.data.meta = _account_controller.data.meta || {};
	_account_controller.data.meta.actions = {
		createPosition: 'top',
		afterCreated: 'clear',
		afterUpdated: 'last'
	};
	res.send(_account_controller);
});
express.get('/api/dashboard', (req, res) => {
	let dashboard_controller = SR.Flexform.controllers['dashboard'];
	dashboard_controller.find();
	dashboard_controller.data.meta = {};
	dashboard_controller.data.meta.actions = {
		createPosition: 'top',
		afterCreated: 'clear'
	};
	console.log('API/DASHBOARD' + JSON.stringify(dashboard_controller));
	res.send(dashboard_controller);
});

express.post('/api/dashboard', (req, res) => {
	let _account_controller = SR.Flexform.controllers['dashboard'];
	let userdata = req.body;
	userdata.account = req.body.account.toLowerCase();
	_account_controller
		.create(userdata)
		.then(() => {
			return res.status(200).send();
		})
		.catch(err => {
			return res.status(400).send(err);
		});
});

/*
express.get('/api/dashboard/:account', (req, res) => {
	SR.API._ACCOUNT_GETDATA(
		{
			account: req.params['account'],
			types: ['control', 'email', 'data']
		},
		(err, result) => {
			if (err) {
				res.send(err + result);
			} else {
				let values = {};
				let userInfo = Object.assign(
					{},
					{
						account: result.account,
						email: result.email,
						roles: result.control.groups
					},
					result.data
				);
				values[result.account] = userInfo;
				res.send({
					name: '_accounts',
					data: {
						fields: [...account_fields, ...extra_fields],
						values
					},
					meta: {
						actions: {
							create: 'top',
							afterCreated: 'clear',
							afterUpdated: 'last'
						}
					}
				});
			}
		}
	);
});

express.patch('/api/dashboard/:account', (req, res) => {
	const account = req.params['account'];
	let {
		account: original_account,
		password: password,
		roles: groups,
		email,
		_confirm_password,
		...data
	} = req.body;
	let userdata = Object.assign(
		{},
		{ data, email },
		{
			control: { groups }
		}
	);
	SR.API._ACCOUNT_SETDATA({ account, data: userdata }, err => {
		if (err) {
			LOG.error(err);
			return res.status(400).send();
		} else if (password) {
			SR.API._ADMIN_ACCOUNT_SETPASS(
				{
					account,
					password
				},
				(err, result) => {
					if (err) {
						LOG.error(err);
						return res.status(400).send();
					} else {
						return res.status(200).send(result);
					}
				}
			);
		} else {
			res.send();
		}
	});
});

express.delete('/api/dashboard/:account', (req, res) => {
	const account = req.params['account'];
	SR.API._ACCOUNT_DELETE({ account }, err => {
		if (err) {
			res.status(400).send();
		} else {
			res.send(200);
		}
	});
});
*/

LOG.sys('Auto-generated dashboard APIs', 'Flexform');
