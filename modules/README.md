# Flexflow

## Work with [Scalra-flexform](https://gitlab.com/imonology/scalra-flexform/-/tree/flexflow)

## Component 架構

-   Flow create

```
.
└── sf_flow_create
    └── sf_tab_view
        └── sf_update
            └── sf_form
                └── sf_sub_form
```

-   Flow review

```
.
└── sf_flow_review
    └── sf_tab_view
        └── sf_update
            └── sf_form
                └── sf_sub_form
```

## 設定文件

-   先行定義好 model，參照[Scalra-flexform](https://gitlab.com/imonology/scalra-flexform/-/tree/flexflow)
-   flow 定義文件
    定義在 `/api/flows/` 目錄底下

    新增 js file 如 `example_flow.js`

    新增完後應會有 `/api/flows/example_flow.js` 檔案

    **該檔案用來設定所有的 flow 相關參數**

-   route 設定加上 flow 的 page `/lobby/router.js`

## flow 定義文件的參數定義方法

<details>

### form

設定該 flow 使用到的表單

example:

```
form: ['form1', 'form2', 'form3'],
```

可以設定多張表單，當有超過 1 張表單時，會使用 tab 分頁顯示

### name

設定該 flow 的名稱

example:

```
name: '簽核系統',
```

### pending_field

設定待處理區顯示的額外欄位

預設顯示

-   系統自動產生的 id
-   狀態
-   審核
    ![](https://i.imgur.com/XIztE31.png)

example:

```
pending_field: ['field1', 'field2'],
```

### done_field

設定已處理區顯示的額外欄位

和 pending_field 相同

example:

```
done_field: ['field1', 'field2'],
```

### start

該 flow 起始的 step id(需定義在 steps 裡面)

example:

```
start: 'step1'
```

### ends

該 flow 結束的 steps(需定義在 steps 裡面)

當進到設定的 ends step 時，該筆表單會被放到已處理

example:

```
ends: ['End', 'Cancel'],
```

### Steps

Steps 定義為一個 Object，key 為 step 的 ID，values 為詳細的設定參數
example:

```
steps: {
    step_a: {...},
    step_b: {...}
}
```

### 每一個 step 裡面的參數

-   title
-   auth

    type: array || function

    設定可以編輯這個 step 的使用者。admin 無論設定如何都可以有編輯權限

    array: 將可以編輯者的 groups 塞入 array 中

    ```
    auth: ['manager', 'user']
    ```

    假如想要更細的設定編輯權限，可以使用自定義 funcion

    function:
    需回傳 null || string
    null 代表有權限，string 則是被黨權限的原因

    ```
    auth: (groups, account_data, forms) => {
        /*
            // input parameter example
            groups: ['manager', 'user'],
    		account_data: { // 在 /api/models/_account.js 裡面設定的資料
    			name: 'Name',
    			...
    		},
            forms:  [
                {
                    form_name: 'example_form_name',
                    record_id: 'example_record_id',
                    values: {
                        field1: '...',
                        field2: '...',
                        ...
                    }
                }
            ]
        */
        if (groups.indexOf('manager') !== -1) {
            return null;
        }
        return '必須為管理者';
    }
    ```

-   shows, hidden
-   lock, unlock
-   option
    審核選項
-   default_option
    預設的審核選項
-   notify:
    尚未實作
-   appoint
    非必需參數
    type: object

    ```
    {
    	condition: (groups, data) => {

    	},
    	groups: [],
    	handler
    }
    ```

    審核時可指定特殊帳號群組 handler，需搭配 handler 參數使用
    當有該 step 有指定 handler 時，只有該特殊帳號群組才可以審核

    -   condition function

            	需回傳 true || false

            	和groups參數擇一使用

            	傳入帳號的groups和data，自訂判斷該帳號是否加入選擇

            	```
            	(groups, data) => {
            		// do something
            		return true;
            	}
            	```

    -   groups
        array of string
        和 condition 參數則依使用
    -   handler
        指定 handler id

-   handler
    非必需參數
    type: string

-   goto:
    必須參數
    type: array of object

    ```
    [
    	{
    		condition: (forms, reviews) => {
    			// do something

    			if (pass) {
    				return true;
    			} else {
    				return false;
    			}
    		})
    	},
    	step: 'next_step'
    ]
    ```

    審核之後，會去檢查 goto 裡面自定義的 condition function

    -   condition function

            	需回傳 true || false

            	預設會帶入兩個參數
            	1. forms
            	```
            	[
            		{
            			form_name: 'example_form_name',
            			record_id: 'example_record_id',
            			values: {
            				field1: '...',
            				field2: '...',
            				...
            			}
            		}
            	]
            	```
            	2. reviews

            	```
            	...
            	```

    -   step

            	當condition回傳為true時，前往該設定的step

-   onDoneStep:

    funciton:

    ```
    (next_step, flow_name, flow_record_id, forms, hander_accounts) => {
    	// do something
    }
    ```

    離開此 step 時呼叫此 function

    </details>

## 設定文件範例

-   完整定義格式如下

<details>

```
module.exports = {
	form: ['document'], // 指定的form或multi-form
	name: '簽核表單',
	/*
	開始的第一個關卡
	這是一個特例，在這一個關卡開始建立一筆新的資料，其他關卡都是使用該筆資料
	*/
	start: 'A',
	ends: ['End', 'Cancel'],
	steps: {
		A: {
			title: '建立表單',
			auth: [],     // 可以看見的group
			shows: ['applicant', 'zipcode'],          // 顯示的欄位
			hidden: [],         // 和shows二選一
			lock: ['applicant'],           // 不可被修改的欄位
			unlock: [],         // 跟lock二選一
			option: ['申請'],     // 審核者選項
			notify: true, 			// 開啟通知功能
			appoint: {groups: ['manager'], handler: 'observer1'}, 	// 顯示指派選項，可依照指定的groups顯示下拉選單，可複選
			goto: 'B',
			/*
			是否需要輸出:
			假如設定為true，會出現button:輸出報表
			*/
			print: false
		},
		B: {
			title: '申請評估',

			auth: ['manager'], 		// 可以修改
			view: ['user', 'c'], 				// 可以看見

			shows: ['applicant', 'item', 'desc1', 'desc2'],          // 顯示的欄位
			hidden: [],         // 和shows二選一
			lock: [],           // 不可被修改的欄位
			unlock: ['desc2'],         // 跟lock二選一

			option: ['通過', '前往B2', '取消'],     // 審核者選項
			notify: true, 			// 開啟通知功能
			/*
				定義可以觀看角色的關聯性，假如其他status也設為observer1, 則這兩邊會是固定的人看到
				簡言之，可以固定觀看的角色，而非是整個group都可以看到
			*/
			handler: 'observer1', // 需搭配	appoint 使用
			goto: [	            // 前往其他關卡
				{
					condition: (form, reviews) => {
						/*
							reviews: [
								{
									option: '通過',
									comment: '...'
								},
							]
						*/
						if (reviews[0].option === '通過')
							return true;
						else
							return false;
					},
					step: 'C'         // 前往的關卡
				},
				{
					condition: (form, reviews) => {
						if (reviews[0].option === '前往B2')
							return true;
						return false;
					},
					step: 'B2'
				},
				{
					condition: (form, reviews) => {
						if (reviews[0].option === '取消')
							return true;
						return false;
					},
					step: 'Cancel'
				}
			],
			/*
			是否需要輸出:
			假如設定為true，會出現button:輸出報表
			*/
			print: false
		},

		B2: {
			title: 'B2 Step',

			auth: ['c'], 		// 可以修改
			view: ['user', 'manager'], 				// 可以看見

			shows: ['applicant', 'item', 'desc1', 'desc2', 'desc4'],          // 顯示的欄位
			hidden: [],         // 和shows二選一
			lock: [],           // 不可被修改的欄位
			unlock: ['desc4'],         // 跟lock二選一
			option: ['通過', '退回'],     // 審核者選項
			notify: true, 			// 開啟通知功能
			/*
				定義可以觀看角色的關聯性，假如其他status也設為observer1, 則這兩邊會是固定的人看到
				簡言之，可以固定觀看的角色，而非是整個group都可以看到
			*/
			handler: 'observer1', // 需搭配	appoint 使用
			goto: [	            // 前往其他關卡
				{
					condition: (form, reviews) => {
						/*
							reviews: [
								{
									option: '通過',
									comment: '...'
								},
							]
						*/
						if (reviews[0].option === '通過')
							return true;
						else
							return false;
					},
					step: 'End'         // 前往的關卡
				},
				{
					condition: (form, reviews) => {
						if (reviews[0].option === '退回')
							return true;
						return false;
					},
					step: 'B'
				}
			],
			/*
			是否需要輸出:
			假如設定為true，會出現button:輸出報表
			*/
			print: false
		},


		C: {
			title: '複查',

			auth: ['manager'], 		// 可以修改
			view: [], 				// 可以看見

			shows: ['applicant', 'item', 'desc1', 'desc2', 'desc3'],          // 顯示的欄位
			hidden: [],         // 和shows二選一
			lock: [],           // 不可被修改的欄位
			unlock: ['desc3'],         // 跟lock二選一
			option: ['通過', '退回'],     // 審核者選項
			notify: true, 			// 開啟通知功能
			/*
				定義可以觀看角色的關聯性，假如其他status也設為observer1, 則這兩邊會是固定的人看到
				簡言之，可以固定觀看的角色，而非是整個group都可以看到
			*/
			handler: 'observer1', // 需搭配	appoint 使用
			goto: [	            // 前往其他關卡
				{
					condition: (form, reviews) => {
						/*
							reviews: [
								{
									option: '通過',
									comment: '...'
								},
							]
						*/
						if (reviews[0].option === '通過')
							return true;
						else
							return false;
					},
					step: 'End'         // 前往的關卡
				},
				{
					condition: (form, reviews) => {
						if (reviews[0].option === '退回')
							return true;
						return false;
					},
					step: 'B'
				}
			],
			/*
			是否需要輸出:
			假如設定為true，會出現button:輸出報表
			*/
			print: false
		},
		End: {
			title: '完成',
			auth: ['manager'],
			shows: [],        	// 顯示的欄位
			hidden: [],         // 和shows二選一
			lock: [],           // 不可被修改的欄位
			unlock: [],         // 跟lock二選一
			print: false
		},
		Cancel: {
			title: '案件取消',
			auth: ['manager'],
			shows: [],        	// 顯示的欄位
			hidden: [],         // 和shows二選一
			lock: [],           // 不可被修改的欄位
			unlock: [],         // 跟lock二選一
			print: false
		}
	}
}
```

</details>

## 設定 route

-   完整定義格式如下

<details>

```
const menu = [
    ...,
    {
        "path": "/multi_form_flow",
        "redirect": "/multi_form_flow",
        "name": "多表單簽合系統",
        "meta": {
            "title": "多表單簽合系統",
            "icon": "multi_form_flow"
        },
        "children": [
            {
                "path": "create",
                "name": "建立表單",
                "type": "flow_create",
                "meta": {
                    "title": "多表單簽合系統",
                    "icon": "create",
                    roles: ['user']
                }
            },
            {
                "path": "pending",
                "name": "待處理",
                "type": "flow_pending",
                "meta": {
                    "title": "待處理",
                    "icon": "pending",
                    roles: ['manager', 'c']
                }
            },
            {
                "path": "done",
                "name": "已處理",
                "type": "flow_done",
                "meta": {
                    "title": "已處理",
                    "icon": "done",
                    roles: ['manager', 'c']
                }
            },
        ]
    },
    {
        "path": "/flow_list",
        "redirect": "/flow_list",
        "name": "已申請表單",
        "meta": {
            "title": "已申請表單",
            "icon": "flow_list"
        },
        "children": [
            {
                "path": "list",
                "name": "已申請表單",
                "type": "flow_user",
                "meta": {
                    "title": "已申請表單",
                    "icon": "list",
                    roles: ['user']
                }
            },

        ]
    },
    ...
```

</details>
