const Controller = require('./controller.js');
const AccountController = require('./account/controller.js');
LOG.sys('Init controllers....');

SR.Flexform = SR.Flexform || {};
SR.Flexform.models = SR.Flexform.models || {};
SR.Flexform.controllers = SR.Flexform.controllers || {};
SR.Flexform.controller = Controller;

UTIL.validatePath(SR.path.join(SR.Settings.PROJECT_PATH, 'api', 'controllers'));
Object.keys(SR.Flexform.models).forEach(name => {
	if (name !== '_account') {
		SR.Flexform.controllers[name] = new Controller(name);
	}
});
SR.Flexform.controllers['_account'] = new AccountController();
LOG.sys('Auto-generated controllers', 'Flexform');
module.exports.controllers = SR.Flexform.controllers;
