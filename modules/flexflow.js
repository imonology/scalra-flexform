const fs = require('fs');
const { getFlowData, getMultiFormsDataDir } = require('./util')
var moment = require('moment');
var l_flow = SR.State.get('FlexFlow');

const express = SR.Module['express'].app;
const bodyParser = require('body-parser');

express.use(
	bodyParser.json({
		limit: '50mb'
	})
);

express.use(
	bodyParser.urlencoded({
		limit: '50mb',
		extended: true
	})
);

const fileUpload = require('express-fileupload');
const l_path = require('path');

var l_accounts = SR.State.get('_accountMap');
let l_form = SR.State.get('FlexFormMap');

let getAccountFromExtra = (args, extra) => {
	if (extra) {
		if (!extra.session._user) {
			return null;
		}
		return extra.session._user.account;
	} else {
		return args.account;
	}
};

SR.API.add(
	'INIT_FLOW',
	{
		_admin: true,
		name: 'string'
	},
	(args, onDone) => {
		var flow_name = 'Flow:' + args.name;
		var flow_models = {};
		flow_models[flow_name] = {
			id: '*string',
			forms: 'object',
			step: 'string',
			status: 'string',
			handlers: 'object',
			creater: 'string',
			ex_datetime: 'string',
			values: 'object'
		};

		SR.DS.init({ models: flow_models }, function (err, ref) {
			if (err) {
				return LOG.error(err, flow_name);
			}

			LOG.warn(`INIT FLOW : ${args.name}`);
			l_flow[args.name] = {
				settings: {},
				data: {}
			};

			l_flow[args.name].data = ref[flow_name];

			onDone(null);
		});
	}
);

SR.API.add(
	'CREATE_FLOW',
	{
		name: 'string',
		forms: 'array',
		include_forms: 'array',
	},
	(args, onDone, extra) => {
		console.log('進入CREATE_FLOW');
		let account = getAccountFromExtra(args, extra);
		let { name, forms, include_forms } = args;
		// 創建新的flow
		if (!l_flow[name]) {
			return onDone('no this flow');
		}

		let copy_value = SR.Flexform.flows[name].copy_value;
		let form_names = SR.Flexform.flows[name].form;
		let flow = SR.Flexform.flows[name];
		let saveHistory = flow.steps[flow.start].saveHistory;

		let update_field_paras = forms;
		for (let form_name of form_names) {
			if (!update_field_paras.find(ele => ele.form_name === form_name)) {
				update_field_paras.push({
					form_name,
					values: {},
					_history: {}
				});
			}
		}

		let import_include_forms = forms;
		for (let obj of include_forms) {
			if (update_field_paras.find(ele => ele.form_name === obj.form_name)) {
				const found = update_field_paras.find(element => element.form_name === obj.form_name);
				if ( Object.keys(found.values).length == 0 && obj.values != undefined ) {
					found.values = obj.values;
				}
			}
		}

		if (saveHistory) {
			for (let form of forms) {
				let history = form._history;
				//console.log('Append _history');
				//console.log(history);
				if (history._history) {
					delete history._history;
				}
				form.values._history = history;
			}
		}

		let all_p = [];
		// 紀錄哪一個欄位有設定unique:true
		var uniqueChangedFieldName = [];

		for (let form_para of update_field_paras) {
			// console.log('form_para ====');
			// console.log(form_para);	
			/*
				field_data: {
				name: '檔案',
				type: 'upload',
				login: true,
				save_path: '/文件/綠保/{id}/',
				file_name: 'doc_file',
				desc: '',
				unique: true,
				must: false,
				show: true,
				required: false,
				id: 'file'
				}
			*/
			// 判斷是否 copy value by reference or not
			if (!(copy_value === false || typeof copy_value === 'undefined')) {
				form_para.record_id = '';
			}

			SR.API.QUERY_FORM({ name: form_para.form_name }, (err, form) => {
				for (let field in form_para.values) {
					let field_data = form.data.fields.find(
						element => element.id === field
					);

					if (
						field_data &&
						field_data.unique === true &&
						!(
							copy_value === false ||
							typeof copy_value === 'undefined'
						)
					) {
						// 更改unique才能建立相同資料
						uniqueChangedFieldName.push(field);
						field_data.unique = false;
					}
				}
			});
			// console.log('uniqueChangedFieldName === ');
			// console.log(uniqueChangedFieldName);
			/*
			form_para:
			  {
				form_name: 'info',
				values: {
				  name: 'Kevin123',
				  sex: 'male',
				  email: 'a@a.com',
				  address: 'abc',
				  account: 'admin'
				},
				record_id: 'xs524zh1iz89zimli39f0v' // 不一定會有
			  },
		*/

			all_p.push(
				new Promise((resolve, reject) => {
					SR.API.UPDATE_FIELD(form_para, (e, r) => {
						if (e) {
							return reject(e);
						}

						// 更改unique才能避免申請帳號時建立相同資料
						SR.API.QUERY_FORM(
							{ name: form_para.form_name },
							(err, _form) => {
								for (let field in form_para.values) {
									let field_data = _form.data.fields.find(
										element => element.id === field
									);
									if (
										field_data &&
										field_data.unique === false &&
										uniqueChangedFieldName.indexOf(
											field
										) !== -1
									) {
										field_data.unique = true;
									}
								}
							}
						);

						// if (
						// 	copy_value === false ||
						// 	typeof copy_value === 'undefined'
						// ) {
						// 檢查是否有檔案，假如有的話，從temp資料夾移動到正確的資料夾
						SR.API.QUERY_FORM(
							{ name: form_para.form_name },
							(err, _form) => {
								for (let field in form_para.values) {
									let field_data = _form.data.fields.find(
										element => element.id === field
									);

									let resetUrlSavepathAndMoveFile = (
										l_field_data,
										data,
										l_field
									) => {
										// console.dir('== resetUrlSavepathAndMoveFile ==');

										if (data) {
											// console.dir('== got data ==');
											let l_save_path = l_field_data.save_path.replace(
												/{id}/g,
												r.record_id
											);
											// console.log('save_path = ' + l_save_path)
											let paths = l_save_path.split('/');
											// 假如path有沒有建立的資料夾，幫忙建立
											// console.dir('== checkFolder ==');
											checkFolder(paths);
											for (let file_obj of data) {
												if ( file_obj != undefined && file_obj.url.indexOf('blob:http') >= 0 ) { // FIXME: the file_obj here is undefined
													// console.dir('if file_obj.url.indexOf blob:http >= 0');
													// console.log('轉移資料')
													// console.log(SR.Settings.UPLOAD_PATH + '/temp/' + file_obj.file_name)
													// console.log(save_path + file_obj.file_name)
													file_obj.url = `/flexform_upload/${form_para.form_name}/${l_field}/${r.record_id}/${file_obj.file_name}`;
													file_obj.save_path =
														l_save_path +
														file_obj.file_name; // 紀錄真正存的位址
													fs.rename(
														SR.Settings.UPLOAD_PATH +
														'/temp/' +
														file_obj.file_name,
														SR.Settings.UPLOAD_PATH +
															l_save_path +
															file_obj.file_name,
														function (err) {
															if (err) {
																return reject(err);
															}
														}
													);
												}
											}
										}
										return data;
									};

									let reSaveUrl = false; // 是否需要重寫url

									if (
										field_data &&
										field_data.type === 'upload'
									) {
										reSaveUrl = true;
										/*
												field_data: {
												  name: '檔案',
												  type: 'upload',
												  login: true,
												  save_path: '/文件/綠保/{id}/',
												  file_name: 'doc_file',
												  desc: '',
												  must: false,
												  show: true,
												  required: false,
												  id: 'file'
												}
											*/
										resetUrlSavepathAndMoveFile(
											field_data,
											form_para.values[field],
											field
										);
									} else if (
										field_data &&
										field_data.type === 'sub_model'
									) {
										reSaveUrl = true;
										for (let sub_field in field_data.fields) {
											if (
												field_data.fields[sub_field]
													.type === 'upload'
											) {
												for (let sub_model of form_para
													.values[field]) {
													let data = resetUrlSavepathAndMoveFile(
														field_data.fields[
														sub_field
														],
														sub_model[sub_field],
														sub_field
													);
												}
											}
										}
									}
									// 重新寫入真正存的位址
									if (reSaveUrl) {
										SR.API.UPDATE_FIELD(
											{
												form_name: form_para.form_name,
												values: form_para.values,
												record_id: r.record_id
											},
											e => {
												if (e) {
													return reject(e);
												}
												// console.log('修改完url')
											}
										);
									}
								}

								r.form_name = form_para.form_name;
								resolve(r);
							}
						);
						// }
						// else
						// {
						// 	// console.log('copy_value === ');
						// 	// console.log(copy_value);
						// 	r.form_name = form_para.form_name;
						// 	resolve(r);
						// }
					});
				})
			);
		}
		Promise.all(all_p).then(
			values => {
				forms = {};
				for (let item of values) {
					forms[item.form_name] = item.record_id;
				}

				let flow = SR.Flexform.flows[name];
				let { goto, onDoneStep } = flow.steps[flow.start];
				let record_id = UTIL.createToken();
				let detail_forms = []
				if (onDoneStep) {
					// 取得所有form data
					detail_forms = getMultiFormsDataDir(forms);
					// console.log('call onDoneStep');
					// console.log('create的detail_forms');
					// console.log(detail_forms);
					onDoneStep(goto, name, record_id, detail_forms);
				}
				l_flow[name].data.add(
					{
						id: record_id,
						forms,
						step: goto,
						status: 'pending',
						handlers: {},
						creater: account,
						ex_datetime: '',
						values: {
							reviews: [],
							history: []
						}
					},
					err => {
						if (err) {
							return onDone(err);
						}
						// The second argument will be passed to "output"
						// which is the parameter of "flexflow_callback".
						return onDone(null, {
							record_id, // FIXME: deprecated, using flow_record_id instead
							flow_name: name,
							flow_record_id: record_id,
							forms: detail_forms
						});
					}
				);
			},
			reason => {
				console.log('有error');
				return onDone(reason);
			}
		);
	}
);

let getStep = (flow_name, flow_record_id) => {
	let flow_data = getFlowData(flow_name, flow_record_id);
	if (!flow_data) {
		return null;
	}
	let step = null;
	if (!flow_record_id) {
		step =
			SR.Flexform.flows[flow_name].steps[
			SR.Flexform.flows[flow_name].start
			];
	} else {
		// flow_data.step為step的名稱，非這邊的step
		step = SR.Flexform.flows[flow_name].steps[flow_data.step];
	}
	return step;
};

/*
// Control flow permissions
input: {
	extra: 'api input extra',
	flow_name: 'flow name',
	type: 'create' || 'pending' || 'done'
}
output: true || false 
*/
// 可觀看或編輯
let checkFlowAuth = (extra, flow_name, type, flow_record_id) => {
	if (!extra) {
		// server 端呼叫
		return null;
	}
	// 取得當前帳號的groups
	let groups = !extra.session._user ? [] : extra.session._user.control.groups;
	let account = !extra.session._user ? '' : extra.session._user.account;
	if (groups.indexOf('admin') !== -1) {
		return null;
	}

	// 取得當前的step詳細資料
	let step = getStep(flow_name, flow_record_id);
	let flow_data = getFlowData(flow_name, flow_record_id);
	if (!step) {
		return 'Can not find this step';
	}

	// 假如是創建者
	if (flow_record_id) {
		if (flow_data.creater && flow_data.creater === account) {
			return null;
		}
	}

	let view = [];
	if (type === 'list') {
		view = step.view || [];
	}

	let result = checkEditPermission(extra, flow_name, flow_record_id);
	if (!result) {
		return null;
	} else {
		// 檢察觀看權限
		if (groups.some(v => view.includes(v))) {
			return null;
		} else {
			return result;
		}
		// return groups.some(v => view.includes(v)); // 檢查是否有重疊
	}
};

// 可編輯
let checkEditPermission = (extra, flow_name, flow_record_id) => {
	if (!extra) {
		// server 端呼叫
		return null;
	}
	// 取得當前帳號的groups
	let groups = !extra.session._user ? [] : extra.session._user.control.groups;
	let account = !extra.session._user ? '' : extra.session._user.account;
	let account_data = l_accounts[account] ? l_accounts[account].data : {};
	if (groups.indexOf('admin') !== -1) {
		return null;
	}
	// 取得當前的step詳細資料
	let step = getStep(flow_name, flow_record_id);
	let flow_data = getFlowData(flow_name, flow_record_id);
	if (!step) {
		return 'Can not find this step';
	}

	if (step.handler) {
		// console.log(`step ${step.title} 需要檢查handler`)
		// console.log(`handler名稱為${step.handler}`)
		// console.log('當前的handlers')
		// console.log(flow_data.handlers)

		if (!flow_data.handlers[step.handler]) {
			return 'Can not find step.handler';
		}
		if (flow_data.handlers[step.handler].indexOf(account) !== -1) {
			return null;
		} else {
			return 'not handle';
		}
	}

	if (typeof step.auth === 'function') {
		let forms = {};
		if (flow_record_id) {
			forms = getMultiFormsDataDir(flow_data.forms);
		}
		return step.auth(groups, account_data, forms);
	} else {
		if (step.auth.length === 0) {
			return null;
		}
		if (groups.some(v => step.auth.includes(v))) {
			return null;
		} else {
			return 'You do not have permission.';
		}
		// return groups.some(v => step.auth.includes(v)); // 檢查是否有重疊
	}
};

// 曾經處理過
let checkProcessed = (extra, flow_name, flow_record_id) => {
	if (!extra) {
		// server 端呼叫
		return true;
	}
	let groups = !extra.session._user ? [] : extra.session._user.control.groups;
	if (groups.indexOf('admin') !== -1) {
		return true;
	}
	let account = !extra.session._user ? '' : extra.session._user.account;
	let flow_data = getFlowData(flow_name, flow_record_id);
	for (let step_history of flow_data.values.history) {
		// step_history: 每一步驟的history
		for (let review of step_history.reviews) {
			// review: 該step的其中一個review
			if (review.account === account) {
				return true;
			}
		}
	}
	return false;
};

SR.API.add(
	'GET_FLOW',
	{
		_admin: true,
		flow_name: 'string'
	},
	function (args, onDone) {
		let { flow_name } = args;
		if (!SR.Flexform.flows[flow_name]) {
			return onDone(`Flow ${flow_name} is undefined.`);
		}
		return onDone(null, SR.Flexform.flows[flow_name]);
	}
);

// TODO: 權限設定
SR.API.add(
	'GET_FIELD_CONTROL',
	{
		flow_name: 'string',
		step_name: 'string',
		groups: 'array'
	},
	(args, onDone) => {
		// console.log('start find field');
		let { flow_name, step_name, groups } = args;
		let step = SR.Flexform.flows[flow_name].steps[step_name];
		if (step.field_control) {
			for (let control_data of step.field_control) {
				if (groups.indexOf(control_data.group) !== -1) {
					// console.log('find field control');
					// console.log(control_data.control);
					return onDone(null, control_data.control);
				}
			}
		}

		// console.log('old version field control');
		if ((!step.shows && !step.hidden) || (!step.lock && !step.unlock)) {
			return onDone(
				'Field control settings error. (shows, hidden, lock, unlock)'
			);
		}
		return onDone(null, {
			shows: step.shows,
			hidden: step.hidden,
			lock: step.lock,
			unlock: step.unlock
		});
	}
);

// TODO: 權限設定
SR.API.add(
	'GET_FLOW_CREATE',
	{
		flow_name: 'string'
	},
	(args, onDone, extra) => {
		let account = getAccountFromExtra(args, extra);

		let groups = [];
		if (account) {
			groups = l_accounts[account].control.groups;
		}

		let { flow_name } = args;
		let auth_error = checkFlowAuth(extra, flow_name, 'create');
		if (auth_error) {
			return onDone(auth_error);
		}

		SR.API.GET_FLOW({ flow_name }, (err, flow) => {
			if (err) {
				// should not to here
				return onDone('Get flow detail error');
			}
			if (
				!flow.form ||
				!flow.steps ||
				!flow.start ||
				!flow.steps[flow.start]
			) {
				return onDone('GET_FLOW_CREATE: Missing required parameters.');
			}
			let form_names = flow.forms;
			let step = flow.steps[flow.start];
			let copy_value = flow.copy_value;
			let all_p = [];
			let include_forms = [];

			if (
				step.include_form &&
				step.include_form.length !== 0 &&
				account
			) {
				if (!account) {
					return onDone('please login');
				}

				for (let include of step.include_form) {
					if (form_names.find(ele => ele.form === include.form)) {
						all_p.push(
							new Promise((resolve, reject) => {
								// if need to find newest data
								if (include.last_modify_date_field) {
									var newest_record_id;
									var max_date;
									let query = {};
									query[include.account_field] = account;

									SR.API.QUERY_FORM(
										{
											name: include.form,
											query
										},
										(err, forms) => {
											var value_records =
												forms.data.values;

											for (var record_id in value_records) {
												var values =
													value_records[record_id];
												if (
													typeof max_date ===
													'undefined'
												) {
													newest_record_id = record_id;
													max_date =
														values[
														include
															.last_modify_date_field
														];
												} else {
													if (
														!(
															values[
															include
																.last_modify_date_field
															] === null ||
															values[
															include
																.last_modify_date_field
															] === 'undefined'
														)
													) {
														var mydate1 = new Date(
															max_date
														);
														var mydate2 = new Date(
															values[
															include.last_modify_date_field
															]
														);

														if (
															mydate2.getTime() >
															mydate1.getTime()
														) {
															newest_record_id = record_id;
															max_date =
																values[
																include
																	.last_modify_date_field
																];
														}
													}
												}
											}

											SR.API.QUERY_FORM(
												{
													name: include.form,
													query,
													record_id: newest_record_id
												},
												(e, r) => {
													if (e) {
														return reject(e);
													}
													include_forms.push(
														{
															form_name: include.form,
															record_id: newest_record_id,
															values: r.data.values[newest_record_id]
														});
													if (
														Object.keys(
															r.data.values
														).length === 0
													) {
														resolve({
															form: include.form
														});
													}
													resolve({
														form: include.form,
														record_id: Object.keys(
															r.data.values
														)[0]
													});
												}
											);
										}
									);
								} else {
									let query = {};
									query[include.account_field] = account;
									SR.API.QUERY_FORM(
										{
											name: include.form,
											query
										},
										(e, r) => {
											if (e) {
												return reject(e);
											}
											if (
												Object.keys(r.data.values)
													.length === 0
											) {
												resolve({
													form: include.form
												});
											}
											resolve({
												form: include.form,
												record_id: Object.keys(
													r.data.values
												)[0]
											});
										}
									);
								}
							})
						);
					}
				}
			}

			Promise.all(all_p).then(
				values => {
					// console.log('copy_value ==========' );
					// console.log(copy_value);
					// console.log('after find newest data and resolve ===== ');
					// console.log(values);

					let forms_data = values || [];
					// // 將include from 的required id 清除
					// for (let _item of forms_data) {
					// 	if (copy_value === true) {
					// 		// console.log('after copy_value === true ===== ');
					// 		// console.log(_item);
					// 		_item.record_id = '';
					// 	}
					// }

					for (let form_item of form_names) {
						let temp = forms_data.find(
							ele => ele.form === form_item.form
						);
						if (!temp) {
							// 處理rules function
							if (form_item.rules) {
								for (let key in form_item.rules) {
									for (let rule of form_item.rules[key]) {
										if (rule.validator) {
											rule.validator = rule.validator.toString();
										}
									}
								}
							}
							forms_data.push({
								form: form_item.form,
								label: form_item.label,
								rules: form_item.rules || {}
							});
						} else {
							temp.label = form_item.label;
						}
					}
					// console.log('after change record_id ===== ');
					// console.log(values);

					// 處理哪些表單不需要show

					let current_flow = step;

					form_names = JSON.parse(
						JSON.stringify(SR.Flexform.flows[flow_name].forms)
					);
					SR.API.GET_FIELD_CONTROL(
						{
							flow_name,
							step_name: flow.start,
							groups
						},
						(err, field_control) => {
							if (err) {
								return onDone(err);
							}
							let shows = field_control.shows;
							let hidden = field_control.hidden;

							if (shows.length !== 0) {
								for (
									let i = form_names.length - 1;
									i >= 0;
									i--
								) {
									let form_data = form_names[i];
									let form_name = form_data.form;
									let fields = Object.keys(
										SR.Flexform.models[form_name].fields
									);
									if (!shows.some(v => fields.includes(v))) {
										// 如果該form沒有任何一個field在shows裡面
										forms_data.splice(i, 1);
									}
								}
							} else if (hidden.length !== 0) {
								for (
									let i = form_names.length - 1;
									i >= 0;
									i--
								) {
									let form_data = form_names[i];
									let form_name = form_data.form;
									let fields = Object.keys(
										SR.Flexform.models[form_name].fields
									);
									if (
										!fields.some(v => !hidden.includes(v))
									) {
										// 非只要有一個不在hidden裡面 => 全部都在hidden裡面
										forms_data.splice(i, 1);
									}
								}
							}

							return onDone(null, {
								title: step.title,
								form_names,
								forms_data,
								step,
								field_control,
								copy_value,
								include_forms
							});
						}
					);
				},
				reason => {
					console.log('有error');
					return onDone(reason);
				}
			);
		});
	}
);

SR.API.add(
	'GET_FLOW_STEP',
	{
		flow_name: 'string',
		flow_record_id: 'string'
	},
	(args, onDone, extra) => {
		let { flow_name, flow_record_id } = args;
		let account = getAccountFromExtra(args, extra);
		if (!account) {
			return onDone('Missing account.');
		}
		let groups = l_accounts[account].control.groups;

		let auth_error = checkFlowAuth(
			extra,
			flow_name,
			'pending',
			flow_record_id
		);
		let processed = checkProcessed(extra, flow_name, flow_record_id);
		if (!processed && auth_error) {
			return onDone('You do not have permission.');
		}
		let flow_data = getFlowData(flow_name, flow_record_id);
		if (!flow_data) {
			return onDone('input error!');
		}
		let forms = flow_data.forms;
		let current_step = flow_data.step;
		let r_history = flow_data.values.history;
		// console.log('SR.Flexform.flows[ flow_name ].steps[ current_step ]')
		// console.log(SR.Flexform.flows[ flow_name ].steps[ current_step ])
		let appoint = SR.Flexform.flows[flow_name].steps[current_step].appoint;
		let forms_detail = getMultiFormsDataDir(forms);
		let bottom_button =
			SR.Flexform.flows[flow_name].steps[current_step].bottom_button ||
			[];
		for (let i in bottom_button) {
			let button = bottom_button[i];
			let data = button.generate(forms_detail);
			button.id = 'button_file_' + i;
			button.link = data.link;
			button.fileName = data.fileName;
		}
		/*
	appoint: { // 顯示指派選項，可依照指定的groups顯示下拉選單，可複選
		groups: [{
			value: 'manager',
			label: '管理者',
		}, {
			value: 'c',
			label: 'C'
		}],
		label: { // 顯示帳號的文字
			form: 'users',
			account_field: 'account',
			label_field: 'name'
		},
		handler: 'observer1'
	},
	*/
		// 取得所有form data

		let candidates = [];

		if (appoint) {
			if (appoint.condition) {
				candidates.push({
					label: '請選擇',
					options: []
				});
				for (let account in l_accounts) {
					if (typeof l_accounts[account] !== 'object') {
						// 過濾掉 add
						continue;
					}
					if (
						appoint.condition(
							l_accounts[account].control.groups,
							l_accounts[account].data,
							forms_detail
						)
					) {
						candidates[0].options.push({
							label: l_accounts[account].data.name || account,
							value: account
						});
					}
				}
			} else if (appoint.groups) {
				for (let group of appoint.groups) {
					let temp = {
						label: group.label,
						options: []
					};
					for (let account in l_accounts) {
						if (typeof l_accounts[account] === 'function') {
							continue;
						}
						if (
							l_accounts[account].control.groups.indexOf(
								group.value
							) !== -1
						) {
							temp.options.push({
								value: account,
								label: l_accounts[account].data.name || account
							});
						}
					}
					candidates.push(temp);
				}
			} else {
				return onDone('appoint setting error');
			}
		}

		// 處理header

		let header = [];
		if (SR.Flexform.flows[flow_name].steps[current_step].header) {
			header = SR.Flexform.flows[flow_name].steps[current_step].header;
		}

		let l_form = SR.State.get('FlexFormMap');
		let form_names = Object.keys(forms);
		let all_field = {};
		for (let form_id in l_form) {
			let form = l_form[form_id];
			if (form_names.indexOf(form.name) !== -1) {
				let values = form.data.values[forms[form.name]];
				all_field = Object.assign(all_field, values);
			}
		}
		for (let item of header) {
			item.value = all_field[item.field] || '';
		}
		// 處理review_history格式
		let review_history = [];
		for (let step_review of r_history) {
			for (let review of step_review.reviews) {
				review_history.push({
					datetime: review.review_time,
					step:
						SR.Flexform.flows[flow_name].steps[step_review.step]
							.title,
					reviewer: review.account,
					option: review.review.option,
					comment: review.review.comment || ''
				});
			}
		}

		// 處理rules function
		form_names = SR.Flexform.flows[flow_name].forms;
		for (let form_item of form_names) {
			if (form_item.rules) {
				for (let key in form_item.rules) {
					for (let rule of form_item.rules[key]) {
						if (rule.validator) {
							rule.validator = rule.validator.toString();
						}
					}
				}
			}
		}

		// 處理哪些表單不需要show
		let current_flow = SR.Flexform.flows[flow_name].steps[current_step];
		form_names = JSON.parse(
			JSON.stringify(SR.Flexform.flows[flow_name].forms)
		);

		SR.API.GET_FIELD_CONTROL(
			{
				flow_name,
				step_name: current_step,
				groups
			},
			(err, field_control) => {
				if (err) {
					return onDone(err);
				}
				let shows = field_control.shows;
				let hidden = field_control.hidden;
				if (shows.length !== 0) {
					for (let i = form_names.length - 1; i >= 0; i--) {
						let form_data = form_names[i];
						let form_name = form_data.form;
						let fields = Object.keys(
							SR.Flexform.models[form_name].fields
						);
						if (!shows.some(v => fields.includes(v))) {
							// 如果該form沒有任何一個field在shows裡面
							form_names.splice(i, 1);
						}
					}
				} else if (hidden.length !== 0) {
					for (let i = form_names.length - 1; i >= 0; i--) {
						let form_data = form_names[i];
						let form_name = form_data.form;
						let fields = Object.keys(
							SR.Flexform.models[form_name].fields
						);
						if (!fields.some(v => !hidden.includes(v))) {
							// 非只要有一個不在hidden裡面 => 全部都在hidden裡面
							form_names.splice(i, 1);
						}
					}
				}

				let can_eddit = checkEditPermission(
					extra,
					flow_name,
					flow_record_id
				);

				return onDone(null, {
					forms,
					header,
					title:
						SR.Flexform.flows[flow_name].steps[current_step].title,
					current_step,
					review_history,
					candidates,
					flow: SR.Flexform.flows[flow_name],
					form_names,
					bottom_button,
					editPermission: !can_eddit ? true : false,
					field_control
				});
			}
		);

		/*
	candidates: [{
		label: 'Manager',
		options: [{
			value: 'kevin',
			label: 'Kevin'
		}, {
			value: 'ken',
			label: 'Ken'
		}]
	}, {
		label: 'User',
		options: [{
			value: 'sara',
			label: 'Sara'
		}, {
			value: 'kenny',
			label: 'Kenny'
		}]
	}],
	*/
	}
);

// 顯示 pending 或 done 的flow list
SR.API.add(
	'GET_FLOW_LIST',
	{
		flow_name: 'string',
		status: 'string' // pending || processed || done
	},
	(args, onDone, extra) => {
		console.log('GET_FLOW_LIST');
		let { flow_name, status } = args;
		// console.log(l_flow[args.flow_name].data['ja3b09lqnbkkp508jgaori'].status);
		if (!l_flow[flow_name]) {
			return onDone('no this flow');
		}

		let flow_datas = JSON.parse(JSON.stringify(getFlowData(flow_name)));
		let result = {};
		let push_result = record_id => {
			result[record_id] = flow_datas[record_id];
			result[record_id].title =
				SR.Flexform.flows[flow_name].steps[
					result[record_id].step
				].title;
		};
		for (let record_id in flow_datas) {
			if (typeof flow_datas[record_id] === 'function') {
				continue;
			}
			// let auth = checkFlowAuth(extra, flow_name, 'list', record_id); // 可觀看，編輯
			let editable = checkEditPermission(extra, flow_name, record_id); // 可編輯
			let processed = checkProcessed(extra, flow_name, record_id); // 處理過
			if (status === 'pending') {
				if (flow_datas[record_id].status === 'done') {
					continue;
				}
				if (editable) {
					continue;
				}
				push_result(record_id);
			} else if (status === 'processed') {
				if (flow_datas[record_id].status === 'done') {
					continue;
				}
				if (!processed || !editable) {
					continue;
				}
				push_result(record_id);
			} else if (status === 'done') {
				if (flow_datas[record_id].status === 'pending') {
					continue;
				}
				if (!processed) {
					continue;
				}
				push_result(record_id);
			}
		}
		// 取得所有forms的detail
		let form_names = [];
		let all_p = [];
		for (let record_id in result) {
			let forms = result[record_id].forms;
			form_names = Object.keys(forms);
			all_p.push(
				new Promise((resolve, reject) => {
					let forms_detail = getMultiFormsDataDir(forms);

					result[record_id].forms_detail = forms_detail;
					resolve();
				})
			);
		}

		Promise.all(all_p).then(
			() => {
				// 整理顯示的field
				let showField = [];
				if (status === 'pending') {
					showField =
						SR.Flexform.flows[flow_name].pending_field || [];
				} else if (status === 'processed') {
					showField =
						SR.Flexform.flows[flow_name].processed_field || [];
				} else if (status === 'done') {
					showField = SR.Flexform.flows[flow_name].done_field || [];
				}

				let fields = [];
				for (let field of showField) {
					for (let form_name of form_names) {
						if (SR.Flexform.models[form_name].fields[field]) {
							fields.push({
								id: field,
								form_name,
								name:
									SR.Flexform.models[form_name].fields[field]
										.name
							});
							break;
						}
					}
				}

				if (showField.length === 0) {
					fields.push({
						id: 'id',
						name: 'ID'
					});
				}
				fields.push({
					id: 'step',
					name: '狀態'
				});

				// 整理list
				let list = [];
				for (let flow_record_id in result) {
					let temp = {
						id: flow_record_id,
						step: result[flow_record_id].title,
						review: ''
					};
					for (let field of fields) {
						if (field.id === 'id' || field.id === 'step') {
							continue;
						}
						// 找到對應的field資料
						temp[field.id] = result[
							flow_record_id
						].forms_detail.find(
							element => element.form_name === field.form_name
						).values[field.id];
					}
					list.push(temp);
				}
				return onDone(null, {
					data: result,
					list,
					fields
				});
			},
			error => {
				onDone(error);
			}
		);
	}
);

SR.API.add(
	'FLOW_REVIEW',
	{
		_login: true,
		flow_name: 'string',
		flow_record_id: 'string',
		_history: '+array',
		forms: 'array',
		review: '+object',
		handler_accounts: '+array'
	},
	async (args, onDone, extra) => {
		let account = getAccountFromExtra(args, extra);
		let {
			flow_name,
			flow_record_id,
			forms,
			review,
			handler_accounts
		} = args;
		console.dir('=== FLOW_REVIEW handler_accounts ===')
		console.dir(handler_accounts)
		let auth_error = checkFlowAuth(
			extra,
			flow_name,
			'pending',
			flow_record_id
		);
		if (auth_error) {
			return onDone(auth_error);
		}
		let flow_data = getFlowData(flow_name, flow_record_id);
		if (!flow_data) {
			return onDone('input error!');
		}
		let current_step = flow_data.step;
		let handlers = {};
		let handler = null;
		// console.log(SR.Flexform.flows[ flow_name ].steps[ current_step ])
		if (handler_accounts && handler_accounts.length > 0) {
			handler =
				SR.Flexform.flows[flow_name].steps[current_step].appoint
					.handler;
			handlers[handler] = handler_accounts;
			// console.log(SR.Flexform.flows[ flow_name ].steps[ current_step ].appoint.handler)
		}

		// 0.record history
		let step = getStep(flow_name, flow_record_id);

		let saveHistory = step.saveHistory;
		if (saveHistory) {
			// check saveHistory flag on
			for (let form of forms) {
				let history = {}
				if (form._history != undefined) {
					history = form._history;
				}

				if (history._history) {
					delete history._history;
				}

				form.values._history = history;
			}
		}
		// console.log('FLOW_REVIEW flow_data');
		// let copy_value = SR.Flexform.flows[flow_name].copy_value;
		// console.log(copy_value);
		// console.log('FLOW_REVIEW start to save data');
		// 1.紀錄form
		var uniqueChangedFieldName = [];
		for (let form of forms) {
			/*
			form = {
				form_name: 'string',
				record_id: 'string' || null,
				values: {...}
			}
			*/
			// console.log('==================================');
			// console.log( form );
			// 不確定這邊改了會不會對其他地方有影響
			await new Promise(resolve => {
				SR.API.QUERY_FORM({ name: form.form_name }, (err, _form) => {
					// 忽略掉 upload的部分 (包含form & sub_form)
					// console.log('準被忽略掉upload');
					for (let field_data of _form.data.fields) {
						if (field_data.type === 'upload') {
							// console.log(
							// 	'找到upload ' + field_data.id + ' 並且刪除'
							// );
							// console.log(form.values[field_data.id]);
							// let _data = form.values[field_data.id];
							// 	// _data是新的資料，把sub model內的upload用舊資料代替
							// 	_data = _form.data.values[form.record_id][field_data.id];
							delete form.values[field_data.id];
						}
						if (field_data.type === 'sub_model') {
							// console.log('找到sub_model ' + field_data.id);
							for (let sub_field in field_data.fields) {
								let sub_field_data =
									field_data.fields[sub_field];
								if (sub_field_data.type === 'upload') {
									// console.log(
									// 	'找到sub model 裡面有upload ' +
									// 		sub_field
									// );
									/*
										form: {
											sub_field: [
												{
													sub_file: [
														{
															name: '..',
															...
														}
													]
												},
												{
													sub_file: [
														{
															name: '..',
															...
														}
													]
												},
											]
										}
									*/
									if (form.values[field_data.id]) {
										for (let _index in form.values[field_data.id]) {
											let _data = form.values[field_data.id][_index];

											if (_form.data.values[form.record_id] != undefined &&
												_form.data.values[form.record_id][field_data.id] != undefined &&
												_form.data.values[form.record_id][field_data.id][_index] != undefined &&
												_form.data.values[form.record_id][field_data.id][_index][sub_field] != undefined &&
												_form.data.values[form.record_id][field_data.id][_index][sub_field] !== '') {
												// _data是新的資料，把sub model內的upload用舊資料代替
												_data[sub_field] = _form.data.values[form.record_id][field_data.id][_index][sub_field];
											}

											//console.log('_form.data.values[form.record_id][field_data.id][_index][sub_field] ==================================');
											//console.log( _form.data.values[form.record_id][field_data.id][_index][sub_field] );
											/*
																							if (typeof _form.data.values[
																								form.record_id
																								][field_data.id][_index][
																									sub_field
																								] !== 'undefined' &&
																								_form.data.values[
																									form.record_id
																								][field_data.id][_index][
																									sub_field
																								] !== null &&
																								_form.data.values[
																									form.record_id
																								][field_data.id][_index][
																									sub_field
																								] !== '') 
																							{
																								// _data是新的資料，把sub model內的upload用舊資料代替
																								_data[sub_field] =
																									_form.data.values[
																										form.record_id
																									][field_data.id][_index][
																										sub_field
																									];
																							}
											*/
										}
									}
								}
							}
						}
					}
					for (var i = 0; i < _form.data.fields.length; i++) {
						let field_data = _form.data.fields[i];
						if (field_data && field_data.unique === true) {
							// 更改unique才能建立相同資料
							uniqueChangedFieldName.push(field_data.id);
							field_data.unique = false;
						}
					}
					SR.API.UPDATE_FIELD(form, (e, r) => {
						if (e) {
							return onDone(e);
						}
						// 更改unique才能避免申請帳號時建立相同資料
						for (var i = 0; i < _form.data.fields.length; i++) {
							let field_data = _form.data.fields[i];
							if (
								field_data &&
								field_data.unique === false &&
								uniqueChangedFieldName.indexOf(
									field_data.id
								) !== -1
							) {
								field_data.unique = true;
							}
						}
						resolve(r);
					});
				});
			});
		}

		if (!review) {
			return onDone(null);
		}
		// 2.紀錄review
		flow_data.values.reviews.push({
			account,
			review,
			review_time: new moment().format('YYYY-MM-DD HH:mm')
		});
		Object.assign(flow_data.handlers, handlers);
		console.dir('=== after object assign, flow_data.handlers ===');
		console.dir(flow_data.handlers)
		flow_data.sync(function(err) {
			if (err) {
				return onDone(err);
			}
			// console.log('成功')
			SR.API.FLOW_GOTO_NEXT_STEP(
				{
					flow_name,
					flow_record_id,
					handler_accounts
				},
				() => {
					console.log('成功');
					return onDone(null);
				}
			);
		});
	}
);

/*
input: {
	'form1': 'record_id1',
	'form2': 'record_id2'
}

output: {
	[
		{
			form_name: 'form1',
			record_id: 'record_id1',
			values: {
				...
			}
		},
		{
			form_name: 'form2',
			record_id: 'record_id2',
			values: {
				...
			}
		},
	]
}
*/
SR.API.add(
	'FLOW_GOTO_NEXT_STEP',
	{
		flow_name: 'string',
		flow_record_id: 'string',
		handler_accounts: '+array'
	},
	(args, onDone, extra) => {
		console.dir('FLOW_GOTO_NEXT_STEP');
		console.dir(args);
		let { flow_name, flow_record_id, handler_accounts } = args;
		let auth_error = checkFlowAuth(
			extra,
			flow_name,
			'pending',
			flow_record_id
		);
		if (auth_error) {
			return onDone(auth_error);
		}
		let flow_data = getFlowData(flow_name, flow_record_id);
		if (!flow_data) {
			return onDone('input error!');
		}
		let step = flow_data.step;
		let flow = SR.Flexform.flows[flow_name];

		if (!flow.steps[step] || !flow.steps[step].goto) {
			return onDone('FLOW_GOTO_NEXT_STEP error');
		}
		let { goto, onDoneStep } = flow.steps[step];

		// 取得所有form data
		let forms = getMultiFormsDataDir(flow_data.forms);

		// form = form.data.values[ Object.keys(form.data.values)[0] ];
		let reviews = flow_data.values.reviews;
		let temp_reviews = [];
		for (let obj of reviews) {
			temp_reviews.push(obj.review);
		}
		for (let item of goto) {
			if (
				item.condition(forms, temp_reviews, flow_name, flow_record_id)
			) {
				// 通過step的客製化function
				if (onDoneStep) {
					onDoneStep(
						item.step,
						flow_name,
						flow_record_id,
						forms,
						handler_accounts
					);
					// Generic logic
				}

				// 滿足條件，前往 item.step
				flow_data.values.history.push({
					step: flow_data.step,
					reviews: flow_data.values.reviews
				});
				flow_data.values.reviews = [];
				flow_data.step = item.step;
				// 處理完成，改為已結案
				if (flow.ends.indexOf(flow_data.step) !== -1) {
					flow_data.status = 'done';
				}
				flow_data.sync(function (err) {
					if (err) {
						return onDone(err);
					}
					return onDone(null);
				});
				break;
			}
		}
	}
);

SR.API.add('GET_MY_CREATED_FLOW', {}, (args, onDone, extra) => {
	let account = getAccountFromExtra(args, extra);
	console.log('Call GET_MY_CREATED_FLOW');
	// console.log(SR.Flexform.flows);
	// console.log(l_flow)
	let result = [];

	for (let flow_id in l_flow) {
		let flow_data = l_flow[flow_id].data;
		for (let record_id in flow_data) {
			if (typeof flow_data[record_id] === 'function') {
				continue;
			}
			if (flow_data[record_id].creater === account) {
				let setp_id = l_flow[flow_id].data[record_id].step;

				result.push({
					flow_id,
					record_id,
					flow_name: SR.Flexform.flows[flow_id].name,
					step: SR.Flexform.flows[flow_id].steps[setp_id].title
				});
			}
		}
	}
	// console.log(result);
	return onDone(null, result);
});

SR.API.add(
	'FLOW_UPDATE_CREATER',
	{
		flow_name: 'string',
		flow_record_id: 'string',
		creater: 'string'
	},
	(args, onDone) => {
		let { flow_name, flow_record_id, creater } = args;
		let flow_data = getFlowData(flow_name, flow_record_id);
		if (!flow_data) {
			return onDone('input error!');
		}
		flow_data.creater = creater;
		flow_data.sync(function (err) {
			if (err) {
				return onDone(err);
			}
			return onDone(null);
		});
	}
);

let flowDir = SR.path.join(SR.Settings.PROJECT_PATH, 'api', 'flows');
// hanlder flows...
console.log('handler flows...');
let fileList = fs.readdirSync(flowDir).reduce(function (list, file) {
	let path = SR.path.join(flowDir, file);
	let name = file;
	let isDir = fs.statSync(path).isDirectory();
	if (!isDir) {
		return list.concat([{ path, name }]);
	} else {
		return list;
	}
}, []);
SR.Flexform = SR.Flexform || {};
SR.Flexform.flows = SR.Flexform.flows || {};
fileList.forEach(flow => {
	SR.Flexform.flows[flow.name.split('.')[0]] = require(flow.path);
});
Object.keys(SR.Flexform.flows).forEach(name => {
	// console.log('Flows');
	// console.log(name);

	SR.API.INIT_FLOW({ name });
});

var l_getSession = function (req) {
	var cookie = SR.REST.getCookie(req.headers.cookie);
	var session = SR.EventManager.getSession(cookie);
	return session;
};

var l_checkLogin = function (req) {
	var session = l_getSession(req);
	if (session.hasOwnProperty('_user')) {
		var login = session._user;
		login.admin = session._user.account === 'admin';
		return login;
	}

	LOG.warn('user not yet logined...');
	return { control: { groups: [], permissions: [] } };
};

let checkFolder = paths => {
	let dir = SR.Settings.UPLOAD_PATH;

	for (let path of paths) {
		dir = dir + '/' + path;
		if (!fs.existsSync(dir)) {
			fs.mkdirSync(dir);
		}
	}
};

express.use(
	fileUpload({
		tempFileDir: '/upload/'
	})
);

let temp_file = [];

let flexform_upload = req => {
	return new Promise((resolve, reject) => {
		// NOTE: 'field' may be a main_field or a sub_field
		let { form_name, field, id, sub_index } = req.params;
		let sub_field = null;
		if (!form_name || !field) {
			return reject('Missing required data');
		}

		if (!SR.Flexform.models[form_name]) {
			return reject('input data error');
		}

		var login = l_checkLogin(req);
		var fields = SR.Flexform.models[form_name].fields;

		// determine if 'field' is a valid main_field or sub_field name
		var valid_field = false;
		for (let main_field in fields) {
			if (main_field === field) {
				valid_field = true;
				break;
			}

			if (fields[main_field].type === 'sub_model') {
				let sub_form = fields[main_field];
				if (sub_form.fields[field]) {
					sub_field = field;
					field = main_field;
					valid_field = true;
					break;
				}
			}
		}

		if (!valid_field) {
			return reject('invalid field name');
		}

		let field_setting = sub_field
			? fields[field].fields[sub_field]
			: fields[field];

		// 確認是否限制一定要登入，或有用到{account}。是的話，一定要登入
		// TOFIX: account login check shouldn't happen here?
		if (
			(field_setting.login ||
				field_setting.save_path.indexOf('{account}') !== -1) &&
			!login.account
		) {
			return reject('not login');
		}

		let save_path = '';
		let temp_path = field_setting.save_path || '/';
		let file_name = field_setting.file_name || '';

		temp_path = temp_path.replace(/{account}/g, login.account);
		if (id) {
			temp_path = temp_path.replace(/{id}/g, id);
			save_path = temp_path;
		} else {
			save_path = '/temp/';
		}
		let paths = save_path.split('/');

		// create folder if not existing
		checkFolder(paths);
		file_name +=
			'_' + UTIL.createToken() + l_path.extname(req.files.file.name);

		// move uploaded file to upload directory
		req.files.file.mv(
			SR.Settings.UPLOAD_PATH + save_path + file_name,
			err => {
				if (err) {
					console.error(err);
					return reject(err);
				}
				// for new records
				if (!id) {
					temp_file.push({
						name: req.files.file.name,
						file_name,
						save_path: temp_path
					});
					resolve(file_name);
					return;
				}

				SR.API.QUERY_FORM(
					{
						name: form_name,
						record_id: id
					},
					(err, form) => {
						if (err) {
							return reject(err);
						}
						let save_data = {
							name: req.files.file.name,
							file_name,
							url: `/flexform_upload/${form_name}/${field}/${id}/${file_name}`,
							save_path: save_path + file_name
						};

						LOG.warn('save_data before checking sub_field');
						LOG.warn(save_data);

						let values = form.data.values[id];

						if (!sub_field) {
							values[field] = form.data.values[id][field] || [];
							values[field].push(save_data);
						} else {
							save_data.url = `/flexform_upload/${form_name}/${sub_field}/${id}/${file_name}`;

							if (typeof values[field] === 'undefined') {
								values[field] = [];
							}
							if (
								typeof values[field][sub_index] === 'undefined'
							) {
								values[field][sub_index] = {};
							}
							// 假如該欄位沒有任何資料，初始值會是空字串 || If these no data initial value is empty string.
							if (
								typeof values[field][sub_index][sub_field] ===
								'undefined' ||
								!Array.isArray(
									values[field][sub_index][sub_field]
								)
							) {
								values[field][sub_index][sub_field] = [];
							}
							values[field][sub_index][sub_field].push(save_data);
						}

						LOG.warn('save_data after checking sub_field');
						LOG.warn(save_data);
						LOG.warn('values[field]');
						LOG.warn(values[field]);

						SR.API.UPDATE_FIELD(
							{
								form_name,
								record_id: id,
								values
							},
							(err, re) => {
								if (err) {
									return reject(err);
								}

								resolve(file_name);
							}
						);
					}
				);
			}
		);
	});
};

// flexform sub form 上傳檔案
express.post(
	'/api/flexform_upload/:form_name/:field/:id/:sub_index',
	(req, res) => {
		//console.log('sub_index = ');
		//console.log(req.params.sub_index);
		flexform_upload(req).then(file_name => {
			res.send({ success: true, file_name });
		});
	}
);

// flexform 上傳檔案
express.post('/api/flexform_upload/:form_name/:field/:id', (req, res) => {
	// console.log('有傳record_id');
	flexform_upload(req).then(file_name => {
		res.send({ success: true, file_name });
	});
});

express.post('/api/flexform_upload/:form_name/:field', (req, res) => {
	// console.log('沒有傳record_id');
	flexform_upload(req).then(
		file_name => {
			res.send({ success: true, file_name });
		},
		error => {
			// console.log('reject');
			return res.status(500).send({ error });
		}
	);
});

//flexform 刪除檔案
express.get(
	'/api/flexform_delete/:form_name/:field/:id/:file_name',
	(req, res) => {
		const { form_name, field, id, file_name } = req.params;
		SR.API.QUERY_FORM(
			{
				name: form_name,
				record_id: id
			},
			(err, form) => {
				if (err) {
					return res.status(500).send(err);
				}
				let values = form.data.values[id];
				let files = values[field] || [];

				// values: {
				// 	"field_sub_data" : [ 
				// 		{
				// 			"farm_illustration" : [ 
				// 				{
				// 					"name" : "Screenshot from 2020-12-02 02-17-32.png",
				// 					"file_name" : "farm_illustration_g0crpz4wagt6x0po5tjxs6.png",
				// 					"url" : "/flexform_upload/crop_appl_GC/farm_illustration/5wo3a0zl844ipivqdidby/farm_illustration_g0crpz4wagt6x0po5tjxs6.png",
				// 					"save_path" : "/文件/綠保/5wo3a0zl844ipivqdidby/作物申請/農場圖/farm_illustration_g0crpz4wagt6x0po5tjxs6.png"
				// 				}, 
				// 				...
				// 			]
				// 		}
				// 	]
				// }

				if (files.length == 0) {
					for (var key in values) {
						if (!Array.isArray(values[key])) {
							continue;
						}
						// values[key] is an array of object
						values[key].map(x => {
							if (typeof x !== 'object') {
								return;
							}
							// x[field] is an array of object
							if ( x[field] != undefined && Array.isArray(x[field])
							    && x[field].findIndex(file_in_subform => file_in_subform.file_name === file_name) != -1 ) {

								// make a reference to values[key][..][field]
								files = x[field];
								return files;
							}
						});
					}
				}

				let index = files.findIndex(
					element => element.file_name === file_name
				);

				if (index === -1) {
					// 可能是只有傳到 temp 但沒有進 DB，確認一下
					const temp_path = SR.Settings.UPLOAD_PATH + '/temp/' + file_name;
					fs.stat(temp_path, (err, stats) => {
						if (err) {
							// console.error(err);
							// no file found here ._.
							// so where's the filename come from?
							return res
								.status(500)
								.send({ error: 'can not find this file' });
						}

						console.info(`file exists: ${JSON.stringify(stats)}`);
						console.info('trying to unlink ...');

						fs.unlink(temp_path, (err) => {
							if (err) {
								throw err;
							}
							console.info('successfully deleted' + temp_path);
						});

						// file found in temp dir, return success anyway
						res.send({ success: true });
					});

				} else {
					files.splice(index, 1);
					SR.API.UPDATE_FIELD(
						{
							form_name,
							record_id: id,
							values
						},
						() => {
							res.send({ success: true });
						}
					);
				}

				
			}
		);
	}
);

// 要檔案
express.get('/flexform_upload/:form_name/:field/:id/:file_name', (req, res) => {
	let { form_name, field, id, file_name } = req.params;
	let sub_field = null;

	SR.API.QUERY_FORM(
		{
			name: form_name,
			record_id: id
		},
		(err, form) => {
			if (err) {
				return res.status(500).send(err);
			}

			var fields = SR.Flexform.models[form_name].fields;
			for (let main_field in fields) {
				if (fields[main_field].type === 'sub_model') {
					let sub_form = fields[main_field];
					if (sub_form.fields[field]) {
						sub_field = field;
						field = main_field;
						break;
					}
				}
			}
			let files = form.data.values[id][field];
			// console.log('sub_field ====');
			// console.log(sub_field);

			if (sub_field &&
				sub_field !== null
			) {
				for (let sub_model of form.data.values[id][field]) {
					// console.log('sub_model[sub_field] ====');
					// console.log(sub_model[sub_field]);
					if (typeof sub_model[sub_field] !== 'undefined' &&
						sub_model[sub_field] !== null &&
						sub_model[sub_field] !== '' &&
						sub_model[sub_field].find(
							ele => ele.file_name === file_name
						)
					) {
						files = sub_model[sub_field];
						break;
					}
				}
			}
			let file = files.find(element => element.file_name === file_name);
			//console.log(file);
			if (!file) {
				return res.status(500).send({});
			}
			let dir = SR.Settings.UPLOAD_PATH + file.save_path;
			res.sendFile(dir);
		}
	);
});

// 修改檔案顯示名稱
SR.API.add(
	'EditFileName',
	{
		form_name: 'string',
		field: 'string',
		record_id: 'string',
		file_name: 'string',
		new_name: 'string'
	},
	(args, onDone) => {
		const { form_name, field, record_id, file_name, new_name } = args;
		SR.API.QUERY_FORM(
			{
				name: form_name,
				record_id: record_id
			},
			(err, form) => {
				if (err) {
					return onDone(err);
				}
				let values = form.data.values[record_id];
				if (!values || !values[field]) {
					return onDone('no this file');
				}
				if (!Array.isArray(values[field])) {
					return onDone('field error');
				}
				let file = values[field].find(
					element => element.file_name === file_name
				);
				file.name = new_name;
				SR.API.UPDATE_FIELD(
					{
						form_name,
						record_id,
						values
					},
					(err, result) => {
						return onDone(err, result);
					}
				);
			}
		);
	}
);

require('./flow_api_generator.js');
