const Controller = require('../controller.js');
const fs = require('fs');
let account_fields = require('../dashboard').account_fields;
let role_path = SR.path.join(SR.Settings.PROJECT_PATH, 'api', '_role.js');
let role_sample_path = SR.path.join(
	SR.Settings.PROJECT_PATH,
	'node_modules',
	'scalra-flexform',
	'modules',
	'_role.js.sample'
);
let roles = {};
if (fs.existsSync(role_path)) {
	roles = require(role_path);
} else {
	fs.copyFileSync(role_sample_path, role_path);
}
let extra_fields_path = SR.path.join(
	SR.Settings.PROJECT_PATH,
	'api',
	'models',
	'dashboard.js'
);
let extra_fields = [];
let _account_keep_words = account_fields.map(f => f.id);
module.exports = class AccountController {
	constructor() {
		this.name = 'dashboard';
		this.data = {};
		if (fs.existsSync(extra_fields_path)) {
			extra_fields = require(extra_fields_path).fields;
			extra_fields = Object.keys(extra_fields)
				.map(k => {
					return Object.assign({}, extra_fields[k], {
						id: k
					});
				})
				.filter(field => {
					return !_account_keep_words.includes(field.id);
				});
		}
		this.data.fields = [...account_fields, ...extra_fields];
		// private member
		Object.defineProperty(this, 'lastAction', {
			enumerable: false,
			writable: true
		});
		Object.defineProperty(this, '_selectedField', {
			enumerable: false,
			writable: true
		});
		this.lastAction = 'init';
	}

	setName(name) {
		this.name = name;
		this.lastAction = 'setName';
	}

	find(args = {}) {
		new Promise((resolve, reject) => {
			let ori_accounts = Object.assign({}, SR.State.get('_accountMap'));
			let accounts = {};
			Object.keys(ori_accounts).forEach(key => {
				if (
					!(
						ori_accounts[key].account &&
						ori_accounts[key].email &&
						ori_accounts[key].control
					)
				) {
					delete ori_accounts[key];
				} else {
					let user = Object.assign(
						{},
						{
							account: ori_accounts[key].account,
							email: ori_accounts[key].email,
							roles: ori_accounts[key].control.groups
						},
						ori_accounts[key].data
					);
					accounts[key] = user;
				}
			});
			this.data.values = accounts;
			resolve();
		});
		this.lastAction = 'find';
		return this;
	}

	findOne(args) {
		let params = { name: this.name, query: args.query };
		if (this._selectField(args)) {
			params.show = this._selectField(args);
		}
		new Promise((resolve, reject) => {
			SR.API.QUERY_FORM(params, (err, result) => {
				if (err) {
					return reject(err);
				}
				this.data.values = {};
				if (Object.keys(result.data.values).length < 1) {
					resolve();
				} else {
					result = Object.assign({}, result);
					let first_key =
						args.query.record_id ||
						Object.keys(result.data.values)[0];
					if (result.data.values[first_key]) {
						this.data.values[first_key] =
							result.data.values[first_key];
					}
					this.data.fields = result.data.fields;
					resolve();
				}
			});
		});
		this.lastAction = 'findOne';
		return this;
	}

	create(obj) {
		return new Promise((resolve, reject) => {
			let groups = [...obj.roles];
			delete obj.roles;
			let { account, password, email, ...data } = obj;
			let user = { account, password, email, data, groups };
			this.lastAction = 'create';

			SR.API._ACCOUNT_REGISTER(user, err => {
				if (err) {
					return reject(err);
				}
				return resolve('ok');
			});
		});
	}

	update(obj) {
		if (!obj.values || !obj.record_id) {
			throw new Error('Missing values or record_id');
		}
		new Promise((resolve, reject) => {
			SR.API.UPDATE_FIELD(
				{
					form_name: this.name,
					values: obj.values,
					record_id: obj.record_id
				},
				err => {
					if (err) {
						reject(err);
					}
				}
			);
		}).then(() => {
			this.find();
		});
		this.lastAction = 'update';
		return this;
	}

	destroy(obj) {
		if (obj.record_id === undefined) {
			throw new Error('Missing record_id');
		}
		new Promise((resolve, reject) => {
			SR.API.DELETE_FIELD(
				{
					form_name: this.name,
					record_id: obj.record_id
				},
				err => {
					if (err) {
						reject(err);
					}
				}
			);
		}).then(() => {
			this.find();
		});
		this.lastAction = 'destory';
		return this;
	}

	populated(selected_fields = []) {
		if (selected_fields.length === 0) {
			selected_fields = this.data.fields.map(field => field.id);
		}
		this.data.fields.forEach(field => {
			if (
				(field.hasOwnProperty('model') ||
					field.hasOwnProperty('collection')) &&
				selected_fields.includes(field.id)
			) {
				let ref_controller = new Controller();
				if (field.hasOwnProperty('model')) {
					ref_controller.setName(field.model);
				}
				if (field.hasOwnProperty('collection')) {
					ref_controller.setName(field.collection);
				}
				ref_controller.find();
				// generate option
				field.option = Object.keys(ref_controller.data.values).map(
					k => {
						return {
							text:
								ref_controller.data.values[k][
									field.option_text
								],
							value: k
						};
					}
				);
				// populate ref data
				if (Object.values(this.data.values).length > 0) {
					Object.values(this.data.values).map(v => {
						if (v.hasOwnProperty(field.id)) {
							let ref_id =
								typeof v[field.id] === 'string'
									? v[field.id]
									: v[field.id].id;
							let ref_data = ref_controller.data.values[ref_id];
							ref_data.id = ref_id;
							v[field.id] = ref_data;
							return v;
						}
					});
				}
			}
		});
		this.lastAction = 'populated';
		return this;
	}

	schema() {
		this.data.fields = [...account_fields, ...extra_fields];
		this.data.values = {};
		this.lastAction = 'schema';
		return this;
	}

	removeFields(fields) {
		if (!fields || fields.length === 0) {
			return this;
		}
		if (typeof fields === 'string') {
			fields = [fields];
		}
		fields.forEach(field => {
			this.data.fields = this.data.fields.filter(f => f.id !== field);
			Object.keys(this.data.values).forEach(
				k => delete this.data.values[k][field]
			);
		});
		this.lastAction = 'removeFields';
		return this;
	}

	hideFields(fields) {
		if (!fields || fields.length === 0) {
			return this;
		}
		if (typeof fields === 'string') {
			fields = [fields];
		}
		fields.forEach(field => {
			this.data.fields = this.data.fields.map(f => {
				if (f.id === field) {
					f.show = false;
				}
				return f;
			});
		});
		this.lastAction = 'hideFields';
		return this;
	}
	_selectField({ select, discard, ...args }) {
		let selectedFields;
		if (!select && discard && discard.length > 0) {
			selectedFields = Object.keys(this.schema().data.fields).filter(
				field => !discard.includes(field)
			);
		}
		if (select && select.length > 0) {
			selectedFields = select;
		}
		return selectedFields;
	}
};
