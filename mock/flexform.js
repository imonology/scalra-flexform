import Mock from 'mockjs';

export default {
	create: () => {
		const fields = Mock.mock({
			id: '38E8186D-F2FB-4BAA-8CE9-A219A9911218',
			flexform_version: '2.0',
			name: 'comment',
			key_field: '',
			data: {
				fields: [
					{
						name: 'users account',
						type: 'string',
						must: true,
						id: 'account'
					},
					{
						name: 'content',
						type: 'string',
						must: true,
						id: 'content'
					}
				],
				values: {
					'006gz6zrl7kjpvg2z9lljtpq': {
						account: 'xcv',
						content: 'sfd'
					},
					de5jm9nfqijr1f3hb68qqr: {
						account: 'lemon',
						content: 'ookk'
					}
				}
			},
			_id: '5cbfa1472df1557a2b444f36'
		});
		return fields;
	}
};
