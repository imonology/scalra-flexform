import Mock from 'mockjs';

export default {
	getMenu: () => {
		const items = Mock.mock([
			{
				path: '/',
				redirect: '/dashboard',
				name: 'Dashboard',
				hidden: true,
				children: [
					{
						path: 'dashboard'
					}
				]
			},
			{
				path: '/user',
				redirect: '/user',
				name: 'User',
				meta: {
					title: 'User',
					icon: 'User'
				},
				children: [
					{
						path: 'create',
						name: 'create user',
						type: 'create',
						meta: {
							title: 'create user',
							icon: 'user'
						}
					},
					{
						path: 'update',
						name: 'update user',
						meta: {
							title: 'update',
							icon: 'edit'
						}
					},
					{
						path: 'list',
						name: 'list user',
						meta: {
							title: 'list',
							icon: 'edit'
						}
					}
				]
			},
			{
				path: 'external-link',
				children: [
					{
						path: 'https://www.google.com/',
						meta: {
							title: 'External Link',
							icon: 'link'
						}
					}
				]
			}
		]);
		return items;
	}
};
